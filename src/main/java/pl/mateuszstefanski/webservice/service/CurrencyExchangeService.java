package pl.mateuszstefanski.webservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.webservice.webclient.CurrencyExchangeClient;

@RequiredArgsConstructor
@Service
public class CurrencyExchangeService {

    private final CurrencyExchangeClient currencyExchangeClient;
    public float getEuroSingleValue() {
        return currencyExchangeClient.getExchangedCurrency().getRates().get(0).getMid();
    }
}
