package pl.mateuszstefanski.webservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

@RequiredArgsConstructor
@Component
public class CurrencyConverter {

    private final CurrencyExchangeService service;

    public Double convertPlnToEuro(BigDecimal pln){
        var euroExchangeRate = BigDecimal.valueOf(service.getEuroSingleValue());
        var result = pln.divide(euroExchangeRate,2, RoundingMode.HALF_UP);
        return result.doubleValue();
    }
}
