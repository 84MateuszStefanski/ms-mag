package pl.mateuszstefanski.webservice.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
public class Currency implements Serializable {

    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;

}
