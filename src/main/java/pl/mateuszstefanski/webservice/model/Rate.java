package pl.mateuszstefanski.webservice.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
public class Rate implements Serializable {

    private String no;
    private String effectiveDate;
    private float mid;

}
