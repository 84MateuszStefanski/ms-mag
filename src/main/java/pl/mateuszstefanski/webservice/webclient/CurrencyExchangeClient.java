package pl.mateuszstefanski.webservice.webclient;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.mateuszstefanski.webservice.model.Currency;
import pl.mateuszstefanski.webservice.model.Rate;

import java.util.List;

@Component
public class CurrencyExchangeClient {

    private static final String CURRENCY_EXCHANGE_URL = "https://api.nbp.pl/api/exchangerates/rates/a/eur?format=json";
    private RestTemplate restTemplate = new RestTemplate();
    public Currency getExchangedCurrency(){
        var currency = restTemplate.getForObject(CURRENCY_EXCHANGE_URL, Currency.class);
        var newCurrency = new Currency();
        var rate =  new Rate();
        if (currency != null) {
            rate.setNo(currency.getRates().get(0).getNo());
        }
        if (currency != null) {
            rate.setEffectiveDate(currency.getRates().get(0).getEffectiveDate());
        }
        if (currency != null) {
            rate.setMid(currency.getRates().get(0).getMid());
        }

        if (currency != null) {
            newCurrency.setTable(currency.getTable());
        }
        if (currency != null) {
            newCurrency.setCurrency(currency.getCurrency());
        }
        if (currency != null) {
            newCurrency.setCode(currency.getCode());
        }
        newCurrency.setRates(List.of(rate));
        return newCurrency;
    }



}


