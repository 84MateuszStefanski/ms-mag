package pl.mateuszstefanski.domain.customer;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customers")
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    private Long customerId;

    private String name;

    @Enumerated(EnumType.STRING)
    private CustomerType customerType;

    @Column(name = "tax_id", unique=true)
    private String taxIdentityNr;

    private String country;

    private String postCode;

    private String city;

    private String street;

    private String phone;

    private String email;

    private boolean saleLock;

    private BigDecimal permanentDiscount;

    private int daysToPaymentDeadline;

    private String bankAccountNumber;

    private boolean hasHistory;

}
