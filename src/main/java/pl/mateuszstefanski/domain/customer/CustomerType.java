package pl.mateuszstefanski.domain.customer;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CustomerType {

    SUPPLIER("Supplier"),
    CONSUMER("Consumer");

   private final String displayType;


}
