package pl.mateuszstefanski.domain.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TypeOfArticle {

    GOODS("Goods"),
    SERVICES("Services");

    private final String displayType;

}
