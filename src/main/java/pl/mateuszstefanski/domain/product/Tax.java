package pl.mateuszstefanski.domain.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Getter
@RequiredArgsConstructor
public enum Tax {

    TAX23(new BigDecimal("1.23")),
    TAX22(new BigDecimal("1.22")),
    TAX8(new BigDecimal("1.08")),
    TAX7(new BigDecimal("1.07")),
    TAX6(new BigDecimal("1.06")),
    TAX5(new BigDecimal("1.05")),
    TAX4(new BigDecimal("1.04")),
    TAX3(new BigDecimal("1.03")),
    TAX0(BigDecimal.ZERO);


   private final BigDecimal TAX_VALUE;
}
