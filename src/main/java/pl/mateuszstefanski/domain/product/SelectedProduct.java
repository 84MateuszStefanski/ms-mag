package pl.mateuszstefanski.domain.product;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sel_products")
@Entity
public class SelectedProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    private Long selectedProductId;

    private Long productId;

    private Double quantity;

    private BigDecimal sumNet;

    public Long getSelectedProductId() {
        return selectedProductId;
    }

    public Long getProductId() {
        return productId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public BigDecimal getSumNet() {
        return sumNet;
    }
}



