package pl.mateuszstefanski.domain.product;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "products")
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    private Long productId;

    private String name;

    @Column(name = "number", unique=true)
    private String catalogNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeOfArticle typeOfArticle;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "category_id")
    private ProductCategory category;

    @Column(name = "unit")
    private String stockUnit;

    private Double quantity;

    @Column(name = "ean")
    private String eanCode;

    @Enumerated
    private Tax purchaseTaxRate;

    @Enumerated
    private Tax salesTaxRate;

    private BigDecimal purchaseNetPrice;

    private BigDecimal saleMarginNet;

    private BigDecimal salePriceNet;

    private BigDecimal salePriceGross;

    private boolean hasHistory;










}
