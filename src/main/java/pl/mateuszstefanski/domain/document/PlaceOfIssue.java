package pl.mateuszstefanski.domain.document;

import lombok.*;

import javax.persistence.*;

@Builder
@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "place_of_issue")
@Entity
public class PlaceOfIssue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id", length = 1000)
    private Long placeId;


    private String placeOfIssue;
}
