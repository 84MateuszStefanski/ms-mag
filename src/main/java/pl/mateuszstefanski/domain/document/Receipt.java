package pl.mateuszstefanski.domain.document;

import lombok.*;
import pl.mateuszstefanski.domain.product.SelectedProduct;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "new_receipts")
@Entity
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    private Long newReceiptId;

    @Column(name = "num", unique=true)
    private String receiptNumber;

    @Column(name = "city")
    private String placeOfIssue;

    @Column(name = "date")
    private LocalDateTime dateOfIssue;

    @OneToMany(mappedBy = "productId",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    List<SelectedProduct> products = new ArrayList<>();

    private BigDecimal totalSumNet;
    private BigDecimal totalTax;
    private BigDecimal totalSumGross;
}
