package pl.mateuszstefanski.domain.document;

import lombok.*;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.domain.customer.Customer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "purchase_invoices")
@Entity
public class PurchaseInvoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    @Column(name = "id")
    private Long invoiceId;

    @Column(name = "num", unique=true)
    private String invoiceNumber;

    @Column(name = "city")
    private String placeOfIssue;

    @Column(name = "date")
    private LocalDate dateOfIssue;

    private Long buyerId;

    private Long sellerId;

    @OneToMany(mappedBy = "productId",cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    List<SelectedProduct> products = new ArrayList<>();

    private BigDecimal totalSumNet;
    private BigDecimal totalTax;
    private BigDecimal totalSumGross;




}
