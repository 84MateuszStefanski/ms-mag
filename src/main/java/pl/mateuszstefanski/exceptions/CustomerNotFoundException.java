package pl.mateuszstefanski.exceptions;

public class CustomerNotFoundException extends RuntimeException {

    public CustomerNotFoundException(Long customerId){
        super(String.format("Customer with customerId=%d not found",customerId));
    }

    public CustomerNotFoundException(String taxIdNum){
        super(String.format("Customer with taxIdNum=%s not found",taxIdNum));
    }
}
