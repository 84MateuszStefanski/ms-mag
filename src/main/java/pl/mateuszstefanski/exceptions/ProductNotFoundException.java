package pl.mateuszstefanski.exceptions;

public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(Long productId){
        super(String.format("Product with productId=%d not found" ,productId));
    }

    public ProductNotFoundException(String catalogNum){
        super(String.format("Product with catalogNum=%s not found",catalogNum));
    }

}
