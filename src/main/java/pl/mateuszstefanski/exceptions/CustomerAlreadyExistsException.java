package pl.mateuszstefanski.exceptions;

public class CustomerAlreadyExistsException extends RuntimeException {


    public CustomerAlreadyExistsException(Long customerId){
        super(String.format("Customer with customerId=%d",customerId));
    }

    public CustomerAlreadyExistsException(String taxIdNum){
        super(String.format("Customer with taxIdNum=%s",taxIdNum));
    }
}
