package pl.mateuszstefanski.exceptions;

public class CannotBeDeletedException extends RuntimeException {

    public CannotBeDeletedException(){
        super(String.format("The resource cannot be deleted because it already has a sales history"));
    }
}
