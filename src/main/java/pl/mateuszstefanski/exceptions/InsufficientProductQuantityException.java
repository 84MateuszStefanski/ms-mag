package pl.mateuszstefanski.exceptions;

public class InsufficientProductQuantityException extends RuntimeException{

    public InsufficientProductQuantityException(String catalogNum){
        super(String.format("%s In stock quantity is insufficient", catalogNum));
    }
}
