package pl.mateuszstefanski.exceptions;

public class ProductAlreadyExistsException extends RuntimeException {

    public ProductAlreadyExistsException(String catalogNum){
        super(String.format("Product with catalogNum=%s already exists", catalogNum));
    }

    public ProductAlreadyExistsException(Long productId){
        super(String.format("Product with productId=%d already exists", productId));
    }
}
