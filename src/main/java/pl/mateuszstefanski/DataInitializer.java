package pl.mateuszstefanski;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.document.PlaceOfIssue;
import pl.mateuszstefanski.repository.document.PlaceOfIssueRepository;
import pl.mateuszstefanski.security.AccessRule;
import pl.mateuszstefanski.domain.customer.Customer;
import pl.mateuszstefanski.domain.customer.CustomerType;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.domain.product.TypeOfArticle;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.repository.customer.CustomersRepository;
import pl.mateuszstefanski.repository.product.ProductsRepository;
import pl.mateuszstefanski.repository.security.AccessRuleRepository;
import pl.mateuszstefanski.repository.user.UsersRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DataInitializer implements CommandLineRunner {

    private final UsersRepository usersRepository;
    private final CustomersRepository customersRepository;
    private final ProductsRepository productsRepository;
    private final AccessRuleRepository accessRuleRepository;
    private final PlaceOfIssueRepository placeOfIssueRepository;

    private static final List<AccessRule> ACCESS_RULES = new ArrayList<>();

    static {

        var accessRule1 = new AccessRule(HttpMethod.GET, "/users/**", "users:read");
        var accessRule2 = new AccessRule(HttpMethod.PATCH, "/users/data/**", "users:update");
        var accessRule3 = new AccessRule(HttpMethod.DELETE, "/users/**", "users:remove");

        var accessRule4 = new AccessRule(HttpMethod.GET, "/customers/**", "customers:read");
        var accessRule5 = new AccessRule(HttpMethod.POST, "/customers/**", "customers:write");
        var accessRule6 = new AccessRule(HttpMethod.PUT, "/customers/**", "customers:update");
        var accessRule7 = new AccessRule(HttpMethod.DELETE, "/customers/**", "customers:remove");

        var accessRule8 = new AccessRule(HttpMethod.GET, "/new/receipts/**", "receipts:read");
        var accessRule9 = new AccessRule(HttpMethod.POST, "/new/receipts/**", "receipts:write");
        var accessRule10 = new AccessRule(HttpMethod.PATCH, "/new/receipts/**", "receipts:update");
        var accessRule11 = new AccessRule(HttpMethod.DELETE, "/new/receipts/**", "receipts:remove");

        var accessRule12 = new AccessRule(HttpMethod.GET, "/products/**", "products:read");
        var accessRule13 = new AccessRule(HttpMethod.POST, "/products/**", "products:write");
        var accessRule14 = new AccessRule(HttpMethod.PUT, "/products/**", "products:update");
        var accessRule15 = new AccessRule(HttpMethod.DELETE, "/products/**", "products:remove");

        var accessRule16 = new AccessRule(HttpMethod.GET, "/invoices/**", "invoices:read");
        var accessRule17 = new AccessRule(HttpMethod.POST, "/invoices/**", "invoices:write");
        var accessRule18 = new AccessRule(HttpMethod.PATCH, "/invoices/**", "invoices:update");

        var accessRule19 = new AccessRule(HttpMethod.GET, "/doc-config/**", "doc-config:read");
        var accessRule20 = new AccessRule(HttpMethod.POST, "/doc-config/**", "doc-config:write");

        var accessRule21 = new AccessRule(HttpMethod.GET, "/pdf/**", "pdf:read");
       // var accessRule22 = new AccessRule(HttpMethod.GET, "/swagger-ui/**", "swagger:read");

        ACCESS_RULES.addAll(Arrays.asList(
                accessRule1,accessRule2,accessRule3,
                accessRule4, accessRule5,accessRule6,accessRule7,
                accessRule8, accessRule9, accessRule10, accessRule11,
                accessRule12, accessRule13, accessRule14, accessRule15,
                accessRule16, accessRule17, accessRule18,
                accessRule19, accessRule20,
                accessRule21));
    }

    @Override
    public void run(final String... args) throws Exception {
        initializeAccessRules();
        //initializeUser();
        initializeCustomer();
        initializeProduct();
        initializePlaceOfIssue();
    }

    private void initializeAccessRules() {
        accessRuleRepository.saveAll(ACCESS_RULES);
    }

    private void initializeUser(){
        var user = User.builder()
                .login("userLogin")
                .password("password")
                .name("New User")
                .taxIdentityNr("118-235-55-25")
                .postCode("05-123")
                .city("Downtown")
                .street("Elm Street 3")
                .bankAccountNumber("10 1020 0000 0000 1254 8965")
                .email("user@user.pl")
                .phoneNumber("555-236-987")
                .authorities(getRules())
                .build();
        usersRepository.save(user);
    }

    private void initializeCustomer(){
        var customer = Customer.builder()
                .name("Customer name")
                .customerType(CustomerType.CONSUMER)
                .taxIdentityNr("555-125-87-89")
                .postCode("37-100")
                .city("Uptown")
                .street("Sesame Street 1")
                .country("Wonderland")
                .phone("501-001-002")
                .email("customer@email.com")
                .daysToPaymentDeadline(0)
                .permanentDiscount(BigDecimal.ZERO)
                .bankAccountNumber("24 1085 0000 0000 1050 8899")
                .saleLock(false)
                .hasHistory(false)
                .build();
        customersRepository.save(customer);
    }

    private void initializeProduct(){
        var product = Product.builder()
                .name("Tool set")
                .catalogNumber("YT-38841")
                .category(ProductCategory.builder()
                        .name("Tool-sets")
                        .build())
                .eanCode("123587415")
                .purchaseNetPrice(BigDecimal.valueOf(335.00D))
                .purchaseTaxRate(Tax.TAX23)
                .salesTaxRate(Tax.TAX23)
                .saleMarginNet(BigDecimal.valueOf(20))
                .salePriceNet(BigDecimal.valueOf(402.00D))
                .salePriceGross(BigDecimal.valueOf(494.46))
                .stockUnit("set")
                .quantity(10.0)
                .typeOfArticle(TypeOfArticle.GOODS)
                .hasHistory(false)
                .build();

        var product2 = Product.builder()
                .name("Mini Drill")
                .catalogNumber("YT-82555")
                .category(ProductCategory.builder()
                        .name("Power tools")
                        .build())
                .eanCode("12382555")
                .purchaseNetPrice(BigDecimal.valueOf(35.00D))
                .purchaseTaxRate(Tax.TAX23)
                .salesTaxRate(Tax.TAX23)
                .saleMarginNet(BigDecimal.valueOf(40))
                .salePriceNet(BigDecimal.valueOf(49.00D))
                .salePriceGross(BigDecimal.valueOf(60.23D))
                .stockUnit("set")
                .quantity(25.0)
                .typeOfArticle(TypeOfArticle.GOODS)
                .hasHistory(false)
                .build();

        productsRepository.saveAll(Arrays.asList(product,product2));
    }

    private void initializePlaceOfIssue(){
        var place = PlaceOfIssue.builder()
                .placeOfIssue("Warsaw")
                .build();
        placeOfIssueRepository.save(place);
    }

    private List<String> getRules(){
        return ACCESS_RULES.stream().map(AccessRule::getAuthority).collect(Collectors.toList());
    }
}
