package pl.mateuszstefanski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;;import java.util.Collections;


@EnableSwagger2
@SpringBootApplication
public class MsMagApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsMagApplication.class, args);
    }

    @Bean
    public Docket get() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.ant("/**"))
                .apis(RequestHandlerSelectors.basePackage("pl.mateuszstefanski"))
                .build().apiInfo(createApiInfo());
    }

    private ApiInfo createApiInfo() {
        return new ApiInfo("MS-Mag API",
                "MS-Mag database",
                "1.00",
                "http://mateuszstefanski.pl",
                new Contact("Mateusz", "http://mateuszstefanski.pl", "84.mateuszstefanski@gmail.com"),
                "my own licence",
                "http://mateuszstefanski.pl",
                Collections.emptyList()
        );
    }
}
