package pl.mateuszstefanski.dto.document;

import lombok.Builder;
import lombok.Data;
import pl.mateuszstefanski.dto.product.SelectedProductDto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ReceiptDto {

    @NotNull
    @NotEmpty
    private String receiptNumber;

    @NotNull
    @NotEmpty
    private String placeOfIssue;

    @NotNull
    @NotEmpty
    private LocalDateTime dateOfIssue;

    @NotEmpty
    List<SelectedProductDto> products;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalSumNet;
    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalTax;
    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalSumGross;
    @Digits(integer = 10, fraction = 2)
    private Double totalSumGrossInEuro;
}
