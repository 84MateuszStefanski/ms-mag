package pl.mateuszstefanski.dto.document;

import lombok.Builder;
import lombok.Data;
import pl.mateuszstefanski.dto.customer.CustomerDto;
import pl.mateuszstefanski.dto.product.SelectedProductDto;
import pl.mateuszstefanski.dto.user.UserDto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class SalesInvoiceDto {

    @NotNull
    @NotEmpty
    private String invoiceNumber;

    @NotNull
    @NotEmpty
    private String placeOfIssue;

    @NotNull
    @NotEmpty
    private LocalDate dateOfIssue;

    @NotNull
    private CustomerDto buyer;

    @NotNull
    private UserDto seller;

    @NotEmpty
    List<SelectedProductDto> products;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalSumNet;
    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalTax;
    @Digits(integer = 10, fraction = 2)
    private BigDecimal totalSumGross;
    @Digits(integer = 10, fraction = 2)
    private Double totalSumGrossInEuro;
}
