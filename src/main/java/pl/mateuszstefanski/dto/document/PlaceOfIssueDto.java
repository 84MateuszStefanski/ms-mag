package pl.mateuszstefanski.dto.document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlaceOfIssueDto {

    private String placeOfIssue;

}
