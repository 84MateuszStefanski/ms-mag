package pl.mateuszstefanski.dto.customer;

import lombok.*;
import pl.mateuszstefanski.domain.customer.CustomerType;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class CustomerDto {


    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    private CustomerType customerType;

    private String taxIdentityNr;

    private String country;

    private String postCode;

    private String city;

    private String street;

    private String phone;

    @Email
    private String email;

    private boolean saleLock;

    private Double permanentDiscount;

    private int daysToPaymentDeadline;

    private String bankAccountNumber;

    @Override
    public String toString() {
        return name + "\n " +
               taxIdentityNr +  "\n " +
               postCode + " " + city + " " + "\n " +
                street + " " + "\n ";
    }
}
