package pl.mateuszstefanski.dto.exception;

import lombok.Data;

@Data
public class ErrorResponse {

    private final String message;

}
