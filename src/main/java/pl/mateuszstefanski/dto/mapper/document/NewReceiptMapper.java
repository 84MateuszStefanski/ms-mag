package pl.mateuszstefanski.dto.mapper.document;

import lombok.RequiredArgsConstructor;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.document.Receipt;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.document.ReceiptDto;
import pl.mateuszstefanski.dto.product.SelectedProductDto;
import pl.mateuszstefanski.repository.product.ProductsRepository;

@Component
@RequiredArgsConstructor
@Mapper(componentModel = "spring")
public abstract class NewReceiptMapper {

    ProductsRepository productsRepository;

    public abstract Receipt mapFromDtoToDomain(ReceiptDto receiptDto);

    @InheritInverseConfiguration
    public abstract ReceiptDto mapFromDomainToDto(Receipt receipt);

    @Mapping(target = "catalogNum", expression = "java( productsRepository.findProductByProductId(selectedProduct.getProductId()).get().getCatalogNumber() )")
    @Mapping(target = "productName", expression = "java( productsRepository.findProductByProductId(selectedProduct.getProductId()).get().getName() )")
    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "sumNet" , source = "sumNet")
    @Mapping(target = "sumGross", expression = "java( selectedProduct.getSumNet().multiply(productsRepository.findProductByProductId(selectedProduct.getProductId()).get().getSalesTaxRate().getTAX_VALUE()) )")
    @Mapping(target = "tax", expression = "java( selectedProduct.getSumNet().multiply(productsRepository.findProductByProductId(selectedProduct.getProductId()).get().getSalesTaxRate().getTAX_VALUE()).subtract(selectedProduct.getSumNet()) )")
    public abstract SelectedProductDto mapFromDomainToDto(SelectedProduct selectedProduct);

    @Mapping(target = "productId", expression = "java( productsRepository.findProductByCatalogNumber(selectedProductDto.getCatalogNum()).get().getProductId())")
    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "sumNet" , source = "sumNet")
    public abstract SelectedProduct mapFromDtoToDomain(SelectedProductDto selectedProductDto);

}
