package pl.mateuszstefanski.dto.mapper.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.document.PurchaseInvoice;
import pl.mateuszstefanski.dto.document.PurchaseInvoiceDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.dto.mapper.user.UsersMapper;
import pl.mateuszstefanski.service.customer.CustomersRepoProxyService;
import pl.mateuszstefanski.service.user.UsersRepoProxyService;

@Component
@RequiredArgsConstructor
public class PurchaseInvoiceManualMapper {
    private final UsersMapper usersMapper;
    private final CustomerMapper customerMapper;
    private final UsersRepoProxyService usersRepoProxyService;
    private final CustomersRepoProxyService customersRepoProxyService;
    private final SelectedProductManualMapper mapper;

    public PurchaseInvoice mapFromDtoToDomain(PurchaseInvoiceDto purchaseInvoiceDto){
        if (purchaseInvoiceDto == null){
            throw new IllegalArgumentException("Argument can`t be null.");
        }
        PurchaseInvoice.PurchaseInvoiceBuilder invoice = PurchaseInvoice.builder();

        invoice.invoiceNumber(purchaseInvoiceDto.getInvoiceNumber());
        invoice.dateOfIssue(purchaseInvoiceDto.getDateOfIssue());
        invoice.placeOfIssue(purchaseInvoiceDto.getPlaceOfIssue());
        invoice.products( mapper.selectedProductDtoListToSelectedProductList( purchaseInvoiceDto.getProducts() ) );
        invoice.sellerId(customerMapper.mapFromDtoToDomain(purchaseInvoiceDto.getSeller()).getCustomerId());
        invoice.buyerId(usersMapper.mapFromDtoToDomain(purchaseInvoiceDto.getBuyer()).getUserId());
        invoice.totalSumNet(purchaseInvoiceDto.getTotalSumNet());
        invoice.totalSumGross(purchaseInvoiceDto.getTotalSumGross());
        invoice.totalTax(purchaseInvoiceDto.getTotalTax());

        return invoice.build();

    }

    public PurchaseInvoiceDto mapFromDomainToDto(PurchaseInvoice purchaseInvoice){
        if (purchaseInvoice == null){
            throw new IllegalArgumentException("Argument can`t be null.");
        }
        PurchaseInvoiceDto.PurchaseInvoiceDtoBuilder invoiceDto = PurchaseInvoiceDto.builder();

        invoiceDto.invoiceNumber(purchaseInvoice.getInvoiceNumber());
        invoiceDto.dateOfIssue(purchaseInvoice.getDateOfIssue());
        invoiceDto.placeOfIssue(purchaseInvoice.getPlaceOfIssue());
        invoiceDto.products( mapper.selectedProductListToSelectedProductDtoList( purchaseInvoice.getProducts() ) );

        invoiceDto.seller(customerMapper.mapFromDomainToDto(customersRepoProxyService.getCustomerById(purchaseInvoice.getSellerId())));

        invoiceDto.buyer(usersMapper.mapFromDomainToDto(usersRepoProxyService.getUserById(purchaseInvoice.getBuyerId())));
        invoiceDto.totalSumNet(purchaseInvoice.getTotalSumNet());
        invoiceDto.totalSumGross(purchaseInvoice.getTotalSumGross());
        invoiceDto.totalTax(purchaseInvoice.getTotalTax());

        return invoiceDto.build();
    }

    public PurchaseInvoiceDto mapFromDomainToDtoForIssue(PurchaseInvoice purchaseInvoice){
        if (purchaseInvoice == null){
            throw new IllegalArgumentException("Argument can`t be null.");
        }
        PurchaseInvoiceDto.PurchaseInvoiceDtoBuilder invoiceDto = PurchaseInvoiceDto.builder();

        invoiceDto.invoiceNumber(purchaseInvoice.getInvoiceNumber());
        invoiceDto.dateOfIssue(purchaseInvoice.getDateOfIssue());
        invoiceDto.placeOfIssue(purchaseInvoice.getPlaceOfIssue());
        invoiceDto.products( mapper.selectedProductListToSelectedProductDtoList( purchaseInvoice.getProducts() ) );

        invoiceDto.seller(null);

        invoiceDto.buyer(usersMapper.mapFromDomainToDto(usersRepoProxyService.getUserById(purchaseInvoice.getBuyerId())));
        invoiceDto.totalSumNet(purchaseInvoice.getTotalSumNet());
        invoiceDto.totalSumGross(purchaseInvoice.getTotalSumGross());
        invoiceDto.totalTax(purchaseInvoice.getTotalTax());

        return invoiceDto.build();
    }

}
