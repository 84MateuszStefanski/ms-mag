package pl.mateuszstefanski.dto.mapper.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.document.SalesInvoice;
import pl.mateuszstefanski.dto.document.SalesInvoiceDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.dto.mapper.user.UsersMapper;
import pl.mateuszstefanski.service.customer.CustomersRepoProxyService;
import pl.mateuszstefanski.service.user.UsersRepoProxyService;

@Component
@RequiredArgsConstructor
public class SalesInvoiceManualMapper {

    private final UsersMapper usersMapper;
    private final CustomerMapper customerMapper;
    private final UsersRepoProxyService usersRepoProxyService;
    private final CustomersRepoProxyService customersRepoProxyService;
    private final SelectedProductManualMapper mapper;

    public SalesInvoice mapFromDtoToDomain(SalesInvoiceDto invoiceDto){
        if (invoiceDto == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        SalesInvoice.SalesInvoiceBuilder invoice = SalesInvoice.builder();

        invoice.invoiceNumber(invoiceDto.getInvoiceNumber());
        invoice.dateOfIssue(invoiceDto.getDateOfIssue());
        invoice.placeOfIssue(invoiceDto.getPlaceOfIssue());
        invoice.products( mapper.selectedProductDtoListToSelectedProductList( invoiceDto.getProducts() ) );
        invoice.sellerId(usersMapper.mapFromDtoToDomain(invoiceDto.getSeller()).getUserId());
        invoice.buyerId(customerMapper.mapFromDtoToDomain(invoiceDto.getBuyer()).getCustomerId());
        invoice.totalSumNet(invoiceDto.getTotalSumNet());
        invoice.totalSumGross(invoiceDto.getTotalSumGross());
        invoice.totalTax(invoiceDto.getTotalTax());

        return invoice.build();
    }

    public SalesInvoiceDto mapFromDomainToDto(SalesInvoice salesInvoice){
        if (salesInvoice == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        SalesInvoiceDto.SalesInvoiceDtoBuilder invoiceDto = SalesInvoiceDto.builder();

        invoiceDto.invoiceNumber(salesInvoice.getInvoiceNumber());
        invoiceDto.dateOfIssue(salesInvoice.getDateOfIssue());
        invoiceDto.placeOfIssue(salesInvoice.getPlaceOfIssue());
        invoiceDto.products( mapper.selectedProductListToSelectedProductDtoList( salesInvoice.getProducts() ) );
        invoiceDto.seller(usersMapper.mapFromDomainToDto(usersRepoProxyService.getUserById(salesInvoice.getSellerId())));
        invoiceDto.buyer(customerMapper.mapFromDomainToDto(customersRepoProxyService.getCustomerById(salesInvoice.getBuyerId())));
        invoiceDto.totalSumNet(salesInvoice.getTotalSumNet());
        invoiceDto.totalSumGross(salesInvoice.getTotalSumGross());
        invoiceDto.totalTax(salesInvoice.getTotalTax());

        return invoiceDto.build();

    }

    public SalesInvoiceDto mapFromDomainToDtoForIssue(SalesInvoice salesInvoice){
        if (salesInvoice == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        SalesInvoiceDto.SalesInvoiceDtoBuilder invoiceDto = SalesInvoiceDto.builder();

        invoiceDto.invoiceNumber(salesInvoice.getInvoiceNumber());
        invoiceDto.dateOfIssue(salesInvoice.getDateOfIssue());
        invoiceDto.placeOfIssue(salesInvoice.getPlaceOfIssue());
        invoiceDto.products( mapper.selectedProductListToSelectedProductDtoList( salesInvoice.getProducts() ) );
        invoiceDto.seller(usersMapper.mapFromDomainToDto(usersRepoProxyService.getUserById(salesInvoice.getSellerId())));
        invoiceDto.buyer(null);
        invoiceDto.totalSumNet(salesInvoice.getTotalSumNet());
        invoiceDto.totalSumGross(salesInvoice.getTotalSumGross());
        invoiceDto.totalTax(salesInvoice.getTotalTax());

        return invoiceDto.build();

    }
}
