package pl.mateuszstefanski.dto.mapper.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.document.Receipt;
import pl.mateuszstefanski.dto.document.ReceiptDto;

@Component
@RequiredArgsConstructor
public class NewReceiptManualMapper {
    private final SelectedProductManualMapper mapper;

    public Receipt mapFromDtoToDomain(ReceiptDto receiptDto) {
        if ( receiptDto == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        Receipt.ReceiptBuilder newReceipt = Receipt.builder();

        newReceipt.receiptNumber( receiptDto.getReceiptNumber() );
        newReceipt.placeOfIssue( receiptDto.getPlaceOfIssue() );
        newReceipt.dateOfIssue( receiptDto.getDateOfIssue() );
        newReceipt.products( mapper.selectedProductDtoListToSelectedProductList( receiptDto.getProducts() ) );
        newReceipt.totalSumNet( receiptDto.getTotalSumNet() );
        newReceipt.totalTax( receiptDto.getTotalTax() );
        newReceipt.totalSumGross( receiptDto.getTotalSumGross() );

        return newReceipt.build();
    }

    public ReceiptDto mapFromDomainToDto(Receipt receipt) {
        if ( receipt == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        ReceiptDto.ReceiptDtoBuilder newReceiptDto = ReceiptDto.builder();

        newReceiptDto.receiptNumber( receipt.getReceiptNumber() );
        newReceiptDto.placeOfIssue( receipt.getPlaceOfIssue() );
        newReceiptDto.dateOfIssue( receipt.getDateOfIssue() );
        newReceiptDto.products( mapper.selectedProductListToSelectedProductDtoList( receipt.getProducts() ) );
        newReceiptDto.totalSumNet( receipt.getTotalSumNet() );
        newReceiptDto.totalTax( receipt.getTotalTax() );
        newReceiptDto.totalSumGross( receipt.getTotalSumGross() );

        return newReceiptDto.build();
    }

}
