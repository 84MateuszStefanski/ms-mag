package pl.mateuszstefanski.dto.mapper.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.dto.product.SelectedProductDto;
import pl.mateuszstefanski.exceptions.InsufficientProductQuantityException;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class SelectedProductManualMapper {

    private final ProductsRepoProxyService service;

    public SelectedProduct mapFromProductToSelectedProductForPurchase(String catalogNum, Double quantity){

        if(catalogNum == null || quantity == null){
            throw new IllegalArgumentException("Arguments can`t be null");
        }

        var product = service.getProductByCatalogNum(catalogNum);

        return SelectedProduct.builder()
                .productId(product.getProductId())
                .quantity(quantity)
                .sumNet(product.getSalePriceNet().multiply(BigDecimal.valueOf(quantity)))
                .build();
    }

    public SelectedProduct mapFromProductToSelectedProductForSell(String catalogNum, Double quantity){

        if(catalogNum == null || quantity == null){
            throw new IllegalArgumentException("Arguments can`t be null");
        }

        var product = service.getProductByCatalogNum(catalogNum);

        if (!service.productAvailable(catalogNum, quantity)){
            throw new InsufficientProductQuantityException(catalogNum);
        }

        return SelectedProduct.builder()
                .productId(product.getProductId())
                .quantity(quantity)
                .sumNet(product.getSalePriceNet().multiply(BigDecimal.valueOf(quantity)))
                .build();
    }

    public SelectedProductDto mapFromDomainToDto(SelectedProduct selectedProduct) {
        return getSelectedProductDto(selectedProduct, service);
    }

    static SelectedProductDto getSelectedProductDto(final SelectedProduct selectedProduct, final ProductsRepoProxyService service) {
        if ( selectedProduct == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        SelectedProductDto.SelectedProductDtoBuilder selectedProductDto = SelectedProductDto.builder();
        var product = service.getProductById(selectedProduct.getProductId());


        selectedProductDto.catalogNum( product.getCatalogNumber() );
        selectedProductDto.productName( product.getName() );
        selectedProductDto.quantity( selectedProduct.getQuantity() );
        selectedProductDto.sumNet( selectedProduct.getSumNet() );
        selectedProductDto.sumGross( selectedProduct.getSumNet().multiply(product.getSalesTaxRate().getTAX_VALUE()) );
        selectedProductDto.tax( selectedProduct.getSumNet().multiply(product.getSalesTaxRate().getTAX_VALUE()).subtract(selectedProduct.getSumNet()) );

        return selectedProductDto.build();
    }

    public SelectedProduct mapFromDtoToDomain(SelectedProductDto selectedProductDto) {
        if ( selectedProductDto == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        SelectedProduct.SelectedProductBuilder selectedProduct = SelectedProduct.builder();

        selectedProduct.quantity( selectedProductDto.getQuantity() );
        selectedProduct.sumNet( selectedProductDto.getSumNet() );

        selectedProduct.productId( service.getProductByCatalogNum(selectedProductDto.getCatalogNum()).getProductId() );

        return selectedProduct.build();
    }

    public List<SelectedProduct> selectedProductDtoListToSelectedProductList(List<SelectedProductDto> list) {
        if ( list == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        List<SelectedProduct> list1 = new ArrayList<>( list.size() );
        for ( SelectedProductDto selectedProductDto : list ) {
            list1.add( mapFromDtoToDomain( selectedProductDto ) );
        }

        return list1;
    }

    public List<SelectedProductDto> selectedProductListToSelectedProductDtoList(List<SelectedProduct> list) {
        if ( list == null ) {
            throw new IllegalArgumentException("Argument can`t be null.");
        }

        List<SelectedProductDto> list1 = new ArrayList<>( list.size() );
        for ( SelectedProduct selectedProduct : list ) {
            list1.add( mapFromDomainToDto( selectedProduct ) );
        }

        return list1;
    }

}
