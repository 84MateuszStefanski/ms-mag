package pl.mateuszstefanski.dto.mapper.document;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import pl.mateuszstefanski.domain.document.PlaceOfIssue;
import pl.mateuszstefanski.dto.document.PlaceOfIssueDto;

@Mapper(componentModel = "spring")
public interface PlaceOfIssueMapper {

    PlaceOfIssue mapFromDtoToDomain(PlaceOfIssueDto placeOfIssueDto);

    @InheritInverseConfiguration
    PlaceOfIssueDto mapFromDomainToDto(PlaceOfIssue placeOfIssue);

}
