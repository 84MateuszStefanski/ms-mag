package pl.mateuszstefanski.dto.mapper.user;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.dto.user.UserRegistrationDto;

@Mapper(componentModel = "spring")
public interface UsersRegistrationMapper {

    User mapFromDtoToDomain(UserRegistrationDto userDto);

    @InheritInverseConfiguration
    UserRegistrationDto mapFromDomainToDto(User user);


}
