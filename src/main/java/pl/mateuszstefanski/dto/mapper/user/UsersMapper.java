package pl.mateuszstefanski.dto.mapper.user;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.dto.user.UserDto;
import pl.mateuszstefanski.dto.user.UserRegistrationDto;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    UserRegistrationDto mapFromDomainToRegistrationDto(User user);

    @InheritInverseConfiguration
    User mapFromRegistrationDtoToDomain(UserRegistrationDto userDto);

    UserDto mapFromDomainToDto(User user);

    @InheritInverseConfiguration
    User mapFromDtoToDomain(UserDto userDto);
}
