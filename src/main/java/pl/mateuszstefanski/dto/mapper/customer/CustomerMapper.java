package pl.mateuszstefanski.dto.mapper.customer;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import pl.mateuszstefanski.domain.customer.Customer;
import pl.mateuszstefanski.dto.customer.CustomerDto;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    Customer mapFromDtoToDomain(CustomerDto customerDto);

    @InheritInverseConfiguration
    CustomerDto mapFromDomainToDto(Customer customer);

}
