package pl.mateuszstefanski.dto.mapper.product;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.dto.product.ProductCategoryDto;
import pl.mateuszstefanski.dto.product.ProductDto;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product mapFromDtoToDomain(ProductDto productDto);

    @InheritInverseConfiguration
    ProductDto mapFromDomainToDto(Product product);

    ProductCategory mapFromDtoToDomain(ProductCategoryDto category);

    @InheritInverseConfiguration
    ProductCategoryDto mapFromDomainToDto(ProductCategory category);


}
