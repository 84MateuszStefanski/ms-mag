package pl.mateuszstefanski.dto.user;


import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Data
public class UserRegistrationDto {

    @NotEmpty
    @NotNull
    private String login;

    @NotEmpty
    @NotNull
    @Size(min = 8, max = 1000, message = "Password should have from 8 to 1000 chars")
    private String password;

}
