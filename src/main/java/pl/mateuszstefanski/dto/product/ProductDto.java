package pl.mateuszstefanski.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.domain.product.TypeOfArticle;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ProductDto {

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String catalogNumber;

    @NotNull
    private TypeOfArticle typeOfArticle;

    @NotNull
    private ProductCategory category;

    @NotNull
    private String stockUnit;

    @NotNull
    private Integer quantity;

    private String eanCode;

    @NotNull
    private Tax purchaseTaxRate;

    @NotNull
    private Tax salesTaxRate;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal purchaseNetPrice;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal saleMarginNet;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal salePriceNet;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal salePriceGross;



}
