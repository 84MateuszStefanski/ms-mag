package pl.mateuszstefanski.dto.product;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@Getter
@Setter
public class SelectedProductDto {

    @NotNull
    @NotEmpty
    private String catalogNum;

    @NotNull
    @NotEmpty
    private String productName;

    private @Range(min = 0, message = "Only positive numbers")
    Double quantity;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal sumNet;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal sumGross;

    @Digits(integer = 10, fraction = 2)
    private BigDecimal tax;

    @Override
    public String toString() {
        return catalogNum + " " + productName + " " + quantity + " " + sumGross.doubleValue();

    }
}
