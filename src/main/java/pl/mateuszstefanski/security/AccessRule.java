package pl.mateuszstefanski.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpMethod;

import javax.persistence.Entity;
import javax.persistence.Id;


@Getter
@Setter
@NoArgsConstructor
@Entity(name = "rules")
public class AccessRule {

    @Id
    private String urlPattern;

    private String httpMethod;

    private String authority;

    public AccessRule(HttpMethod httpMethod, String urlPattern, String authority) {
        this.httpMethod = httpMethod.name();
        this.urlPattern = urlPattern;
        this.authority = authority;
    }

}
