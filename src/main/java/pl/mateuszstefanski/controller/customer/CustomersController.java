package pl.mateuszstefanski.controller.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.domain.customer.Customer;
import pl.mateuszstefanski.dto.customer.CustomerDto;
import pl.mateuszstefanski.service.customer.CustomersService;

import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/customers")
public class CustomersController {

    private final CustomersService customersService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public CustomerDto addNewCustomer(@RequestBody CustomerDto customerDto){
        return customersService.addNew(customerDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(path = "/{customerId}")
    public CustomerDto getCustomerById(@PathVariable Long customerId){
        return customersService.getById(customerId);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("tax/{taxNum}")
    public CustomerDto getCustomerByTaxNum(@PathVariable String taxNum){
        return customersService.getByNum(taxNum);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/all")
    public List<CustomerDto> getAllCustomers(){
        return customersService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping( "/{customerId}")
    public CustomerDto updateCustomerById(@PathVariable Long customerId, @RequestBody CustomerDto customerDto){
        return customersService.updateById(customerId, customerDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("tax/{taxNum}")
    public CustomerDto updateCustomerById(@PathVariable String taxNum, @RequestBody CustomerDto customerDto){
        return customersService.updateByNum(taxNum, customerDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{customerId}")
    public void deleteCustomerById(@PathVariable Long customerId){
        customersService.deleteById(customerId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "tax/{taxNum}")
    public void deleteCustomerByTaxNum(@PathVariable String taxNum){
        customersService.deleteByNum(taxNum);
    }

    //todo sprawdzić czy wraz z usunięciem Customera usunięta zostanie tabela commercials

    //todo stworzyć endpoint który zwróci listę customerów mających nazwę zaczynającą się na wpisaną frazę i rozne sortowania



}
