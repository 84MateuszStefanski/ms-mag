package pl.mateuszstefanski.controller.user;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.dto.user.UserDto;
import pl.mateuszstefanski.dto.user.UserRegistrationDto;
import pl.mateuszstefanski.service.user.UserSecuredRegistrationService;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UsersController {

    private final UserSecuredRegistrationService registrationService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public UserRegistrationDto registerNewUser(@RequestBody UserRegistrationDto userDto){
        return registrationService.registerUser(userDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping(path = "/data" ,consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto addUserData(@RequestParam String login, @RequestBody UserDto userDto){
       return registrationService.addData(login, userDto);
    }


}
