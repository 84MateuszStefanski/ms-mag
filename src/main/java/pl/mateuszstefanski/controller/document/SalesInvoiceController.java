package pl.mateuszstefanski.controller.document;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.document.SalesInvoiceDto;
import pl.mateuszstefanski.service.document.SalesInvoiceService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/invoices/sell")
public class SalesInvoiceController {

    private final SalesInvoiceService service;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public SalesInvoiceDto issueNewInvoice(){
        return service.createDocument();
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/buyer/{taxIdentityNum}")
    public SalesInvoiceDto addBuyerToInvoiceFromDatabase(@PathVariable String taxIdentityNum){
        return service.addContractor(taxIdentityNum);
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/product")
    public SalesInvoiceDto addProductToInvoice(@RequestParam String catalogNum, @RequestParam Double quantity){
        return service.addProductToDocument(catalogNum,quantity);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping( "/create")
    public SalesInvoiceDto acceptInvoice(){
        return service.acceptDocument();
    }

}
