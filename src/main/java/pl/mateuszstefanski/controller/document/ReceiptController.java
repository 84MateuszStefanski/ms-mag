package pl.mateuszstefanski.controller.document;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.document.ReceiptDto;
import pl.mateuszstefanski.service.document.ReceiptsService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/receipts")
public class ReceiptController {

    private final ReceiptsService service;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ReceiptDto issueNewReceipt(){
        return service.createDocument();
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/product")
    public ReceiptDto addProductToReceipt(@RequestParam String catalogNum, @RequestParam Double quantity){
        return service.addProductToDocument(catalogNum,quantity);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/create")
    public ReceiptDto acceptReceipt(){
        return service.acceptDocument();
    }

    @GetMapping(path = "/all")
    public List<ReceiptDto> getAllReceipts(){
        return service.getAllReceipts();
    }
}
