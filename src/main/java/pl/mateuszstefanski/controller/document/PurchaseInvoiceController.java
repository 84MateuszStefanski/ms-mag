package pl.mateuszstefanski.controller.document;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.customer.CustomerDto;
import pl.mateuszstefanski.dto.document.PurchaseInvoiceDto;
import pl.mateuszstefanski.service.document.PurchaseInvoiceService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/invoices/purchase")
public class PurchaseInvoiceController {

    private final PurchaseInvoiceService service;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public PurchaseInvoiceDto issueNewInvoice(@RequestParam String invoiceNum){
        return service.createDocument(invoiceNum);
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/seller")
    public PurchaseInvoiceDto addSellerToInvoiceFromDatabase(@RequestParam String taxIdentityNum){
        return service.addContractor(taxIdentityNum);
    }

    @ResponseStatus(HttpStatus.OK)
    @PatchMapping("/product")
    public PurchaseInvoiceDto addProductToInvoice(@RequestParam String catalogNum, @RequestParam Double quantity){
        return service.addProductToDocument(catalogNum,quantity);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping( "/create")
    public PurchaseInvoiceDto acceptInvoice(){
        return service.acceptDocument();
    }

}
