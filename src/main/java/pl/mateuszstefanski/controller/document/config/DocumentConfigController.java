package pl.mateuszstefanski.controller.document.config;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.document.PlaceOfIssueDto;
import pl.mateuszstefanski.service.document.config.DocumentConfigService;

@RequiredArgsConstructor
@RestController
@RequestMapping("/doc-config")
public class DocumentConfigController {

    private final DocumentConfigService configService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("{placeOfIssue}")
    public PlaceOfIssueDto setPlaceOfIssue(@PathVariable String placeOfIssue){
        return configService.setPlaceOfDocumentIssue(placeOfIssue);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public PlaceOfIssueDto getPlaceOfIssue(){
        return configService.getPlaceOfDocumentIssue();
    }
}
