package pl.mateuszstefanski.controller.product;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.service.product.SellingPricesService;


@RequiredArgsConstructor
@RestController
@RequestMapping("/prices")
public class SellingPricesController {

    private final SellingPricesService pricesService;

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/margin/{productId}")
    public ProductDto setPriceByMarginForProductId(@PathVariable Long productId, @RequestParam Double margin){
        return pricesService.setPriceByMarginForProductId(productId,margin);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/margin/{catalogNum}")
    public ProductDto setPriceByMarginForCatalogNum(@PathVariable String catalogNum, @RequestParam Double margin){
        return pricesService.setPriceByMarginForCatalogNum(catalogNum,margin);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/net/{productId}")
    public ProductDto setMarginByPriceNetForProductId(@PathVariable Long productId, @RequestParam Double sellPriceNet){
        return pricesService.setMarginByPriceNetForProductId(productId,sellPriceNet);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/net/{catalogNum}")
    public ProductDto setMarginByPriceNetForCatalogNum(@PathVariable String catalogNum, @RequestParam Double sellPriceNet){
        return pricesService.setMarginByPriceNetForCatalogNum(catalogNum,sellPriceNet);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/gross/{productId}")
    public ProductDto setMarginByPriceGrossForProductId(@PathVariable Long productId, @RequestParam Double sellPriceGross){
        return pricesService.setMarginByPriceGrossForProductId(productId,sellPriceGross);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/gross/{catalogNum}")
    public ProductDto setMarginByPriceGrossForProductId(@PathVariable String catalogNum, @RequestParam Double sellPriceGross){
        return pricesService.setMarginByPriceGrossForCatalogNum(catalogNum,sellPriceGross);
    }

}
