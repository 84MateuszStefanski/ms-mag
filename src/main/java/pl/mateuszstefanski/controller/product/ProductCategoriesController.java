package pl.mateuszstefanski.controller.product;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.dto.product.ProductCategoryDto;
import pl.mateuszstefanski.service.product.ProductCategoriesService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/categories")
public class ProductCategoriesController {

    private final ProductCategoriesService categoriesService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductCategoryDto addNewCategory(@RequestBody ProductCategoryDto categoryDto){
        return categoriesService.addNew(categoryDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductCategoryDto> getAllCategories(){
        return categoriesService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/{categoryId}" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductCategoryDto updateCategoryById(@PathVariable Long categoryId, @RequestBody ProductCategoryDto categoryDto){
        return categoriesService.updateById(categoryId, categoryDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(path = "/{name}" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public ProductCategoryDto updateCategoryByName(@PathVariable String name, @RequestBody ProductCategoryDto categoryDto){
        return categoriesService.updateByNum(name, categoryDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{categoryId}")
    public void deleteCategoryById(@PathVariable Long categoryId){
        categoriesService.deleteById(categoryId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{name}")
    public void deleteCategoryById(@PathVariable String name){
        categoriesService.deleteByNum(name);
    }
}
