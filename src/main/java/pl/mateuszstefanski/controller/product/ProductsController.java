package pl.mateuszstefanski.controller.product;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.service.product.ProductsService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductsController {

    private final ProductsService productsService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ProductDto addNewProduct(@RequestBody ProductDto productDto){
        return productsService.addNew(productDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/id/{productId}")
    public ProductDto getProductById(@PathVariable Long productId){
        return productsService.getById(productId);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/num/{catalogNum}")
    public ProductDto getProductByCatalogNum(@PathVariable String catalogNum){
        return productsService.getByNum(catalogNum);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/all")
    public List<ProductDto> getAllProducts(){
        return productsService.getAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("id/{productId}")
    public ProductDto updateProductById(@PathVariable Long productId, @RequestBody ProductDto productDto){
      return productsService.updateById(productId,productDto);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("num/{catalogNum}")
    public ProductDto updateProductByCatalogNum(@PathVariable String catalogNum, @RequestBody ProductDto productDto){
        return productsService.updateByNum(catalogNum,productDto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/id/{productId}")
    public void deleteProductById(@PathVariable Long productId){
        productsService.deleteById(productId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/num/{catalogNum}")
    public void deleteProductByCatalogNum(@PathVariable String catalogNum){
        productsService.deleteByNum(catalogNum);
    }
}

//todo get products list by category
