package pl.mateuszstefanski.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.mateuszstefanski.dto.exception.ErrorResponse;
import pl.mateuszstefanski.exceptions.*;

@RestControllerAdvice
public class GlobalRestControllerErrorHandler {

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(CustomerAlreadyExistsException.class)
    ErrorResponse handleCustomerAlreadyExistsException(final CustomerAlreadyExistsException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerNotFoundException.class)
    ErrorResponse handleCustomerNotFoundException(CustomerNotFoundException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ProductNotFoundException.class)
    ErrorResponse handleProductNotFoundException(ProductNotFoundException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(ProductAlreadyExistsException.class)
    ErrorResponse handleProductAlreadyExistsException(ProductAlreadyExistsException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(CannotBeDeletedException.class)
    ErrorResponse handleCannotBeDeletedException(CannotBeDeletedException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(InsufficientProductQuantityException.class)
    ErrorResponse handleInsufficientProductQuantityException(InsufficientProductQuantityException exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(WrongPasswordException.class)
    ErrorResponse handleWrongPasswordException(WrongPasswordException exception){
        return new ErrorResponse(exception.getMessage());
    }


}
