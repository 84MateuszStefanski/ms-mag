package pl.mateuszstefanski.repository.product;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.domain.product.Product;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductsRepository extends JpaRepository<Product, Long> {

    Optional<Product> findProductByProductId(Long productId);
    Optional<Product> findProductByCatalogNumber(String catalogNumber);
    @NotNull List<Product> findAll();


    //todo dopisac rozne kombinacje metod do sortowania
}
