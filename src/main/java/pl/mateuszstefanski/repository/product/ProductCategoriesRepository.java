package pl.mateuszstefanski.repository.product;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.mateuszstefanski.domain.product.ProductCategory;

import java.util.List;
import java.util.Optional;

public interface ProductCategoriesRepository extends JpaRepository<ProductCategory, Long> {

    Optional<ProductCategory> findByCategoryId(Long categoryID);

    Optional<ProductCategory> findByName(String name);

    @NotNull List<ProductCategory> findAllBy();

}
