package pl.mateuszstefanski.repository.product;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mateuszstefanski.domain.product.SelectedProduct;

import java.util.Optional;

public interface SelectedProductsRepository extends JpaRepository<SelectedProduct, Long> {

    Optional<SelectedProduct> findSelectedProductBySelectedProductId(Long productId);

}
