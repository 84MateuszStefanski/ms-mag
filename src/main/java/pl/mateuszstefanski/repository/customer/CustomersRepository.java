package pl.mateuszstefanski.repository.customer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.domain.customer.Customer;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomersRepository extends JpaRepository<Customer, Long> {

    Optional<Customer> findCustomerByCustomerId(Long id);

    Optional<Customer> findCustomerByTaxIdentityNr(String taxNum);

    List<Customer> findAll();

}
