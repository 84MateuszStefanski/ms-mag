package pl.mateuszstefanski.repository.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.security.AccessRule;

import java.util.List;

@Repository
public interface AccessRuleRepository extends CrudRepository<AccessRule, String> {

    List<AccessRule> findAll();
}
