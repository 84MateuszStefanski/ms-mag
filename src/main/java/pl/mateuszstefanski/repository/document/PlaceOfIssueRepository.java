package pl.mateuszstefanski.repository.document;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mateuszstefanski.domain.document.PlaceOfIssue;

import java.util.List;

public interface PlaceOfIssueRepository extends JpaRepository<PlaceOfIssue, Long> {

}
