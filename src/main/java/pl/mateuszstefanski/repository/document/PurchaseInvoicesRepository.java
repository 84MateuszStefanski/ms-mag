package pl.mateuszstefanski.repository.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.domain.document.PurchaseInvoice;

@Repository
public interface PurchaseInvoicesRepository extends JpaRepository<PurchaseInvoice, Long> {
}
