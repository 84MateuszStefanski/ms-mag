package pl.mateuszstefanski.repository.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.domain.document.SalesInvoice;

import java.util.List;
import java.util.Optional;

@Repository
public interface SalesInvoicesRepository extends JpaRepository<SalesInvoice,Long> {


}
