package pl.mateuszstefanski.repository.document;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.mateuszstefanski.domain.document.Receipt;

import java.util.Optional;

@Repository
public interface ReceiptsRepository extends JpaRepository<Receipt, Long> {

    Optional<Receipt> findById(Long id);

    Optional<Receipt> findReceiptByReceiptNumber(String num);
}
