package pl.mateuszstefanski.service.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.dto.product.ProductCategoryDto;
import pl.mateuszstefanski.repository.product.ProductCategoriesRepository;

@RequiredArgsConstructor
@Service
public class ProductCategoriesRepoProxyService {

    private final ProductCategoriesRepository categoriesRepository;

    public ProductCategory getCategoryById(Long categoryId){
        return categoriesRepository.findByCategoryId(categoryId).get();
    }

    public ProductCategory getCategoryByName(String name){
        return categoriesRepository.findByName(name).get();
    }

    public ProductCategory updateById(Long categoryId, ProductCategoryDto categoryDto){
        var beforeUpdate = categoriesRepository.findByCategoryId(categoryId).get();
        beforeUpdate.setName(categoryDto.getName());
        var afterUpdate = categoriesRepository.save(beforeUpdate);
        return afterUpdate;
    }

    public ProductCategory updateByName(String name, ProductCategoryDto categoryDto){
        var beforeUpdate = categoriesRepository.findByName(name).get();
        beforeUpdate.setName(categoryDto.getName());
        return categoriesRepository.save(beforeUpdate);

    }

    public ProductCategory saveCategory(ProductCategory category){
        return categoriesRepository.save(category);
    }

}
