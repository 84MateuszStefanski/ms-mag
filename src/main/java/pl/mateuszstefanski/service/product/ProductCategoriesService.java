package pl.mateuszstefanski.service.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.dto.product.ProductCategoryDto;
import pl.mateuszstefanski.dto.mapper.product.ProductMapper;
import pl.mateuszstefanski.repository.product.ProductCategoriesRepository;
import pl.mateuszstefanski.service.DomainServiceInterface;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ProductCategoriesService implements DomainServiceInterface<ProductCategoryDto> {

    private final ProductCategoriesRepoProxyService categoriesUtilityService;
    private final ProductCategoriesRepository categoriesRepository;
    private final ProductMapper mapper;

    @Override
    public ProductCategoryDto addNew(final ProductCategoryDto productCategoryDto) {
        var category = mapper.mapFromDtoToDomain(productCategoryDto);
        categoriesUtilityService.saveCategory(category);
        return productCategoryDto;
    }

    @Override
    public ProductCategoryDto getById(final Long id) {
        return null;
    }

    @Override
    public ProductCategoryDto getByNum(final String num) {
        return null;
    }

    @Override
    public List<ProductCategoryDto> getAll() {
        return categoriesRepository.findAllBy().stream()
                .map(mapper::mapFromDomainToDto)
                .sorted(Comparator.comparing(ProductCategoryDto::getName))
                .collect(Collectors.toList());
    }

    @Override
    public ProductCategoryDto updateById(final Long id, final ProductCategoryDto productCategoryDto) {
        return mapper.mapFromDomainToDto(categoriesUtilityService.updateById(id, productCategoryDto));
    }

    /** String num = String Category Name*/
    @Override
    public ProductCategoryDto updateByNum(final String num, final ProductCategoryDto productCategoryDto) {
        return mapper.mapFromDomainToDto(categoriesUtilityService.updateByName(num, productCategoryDto));
    }

    @Override
    public void deleteById(final Long id) {
        categoriesRepository.deleteById(id);
    }

    /** String num = String Category Name*/
    @Override
    public void deleteByNum(final String num) {
        var categoryToDelete = categoriesUtilityService.getCategoryByName(num);
        categoriesRepository.delete(categoryToDelete);
    }

}
