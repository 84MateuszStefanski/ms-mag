package pl.mateuszstefanski.service.product;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.dto.mapper.product.ProductMapper;
import pl.mateuszstefanski.repository.product.ProductsRepository;
import pl.mateuszstefanski.service.prices.PricesAndTaxCalculator;

import java.math.BigDecimal;

@RequiredArgsConstructor
@Service
public class SellingPricesService {

    private final ProductsRepository productsRepository;
    private final ProductsRepoProxyService productsRepoProxyService;
    private final PricesAndTaxCalculator pricesAndTaxCalculator;
    private final ProductMapper productMapper;


    public ProductDto setPriceByMarginForProductId(final Long productId, final Double margin) {

        var product = productsRepoProxyService.getProductById(productId);

        return getProductDto(pricesAndTaxCalculator.changeMarginFormatDoubleToDecimal(margin), product);
    }

    public ProductDto setPriceByMarginForCatalogNum(final String catalogNum, final Double margin) {
        var product = productsRepoProxyService.getProductByCatalogNum(catalogNum);
        return getProductDto(pricesAndTaxCalculator.changeMarginFormatDoubleToDecimal(margin), product);
    }
    
    
    public ProductDto setMarginByPriceNetForProductId(final Long productId, final Double sellPriceNet) {
        return getProductDto(productId, sellPriceNet);
    }

    public ProductDto setMarginByPriceNetForCatalogNum(final String catalogNum, final Double sellPriceNet) {

        return getProductDto(catalogNum, sellPriceNet);
    }


    public ProductDto setMarginByPriceGrossForProductId(final Long productId,final Double sellPriceGross){
        var product = productsRepoProxyService.getProductById(productId);
        return getProductDto(sellPriceGross, product);
    }

    public ProductDto setMarginByPriceGrossForCatalogNum(final String catalogNum, final Double sellPriceGross) {
        var product = productsRepoProxyService.getProductByCatalogNum(catalogNum);
        return getProductDto(sellPriceGross, product);
    }


    @NotNull
    private ProductDto getProductDto(final Double sellPriceGross, final Product product) {
        var margin =
                pricesAndTaxCalculator
                        .countMarginByPriceGross(
                                product.getPurchaseNetPrice(), pricesAndTaxCalculator.changeDoubleToBigDecimal(sellPriceGross));

        var sellingNetPrice =
                pricesAndTaxCalculator
                        .countPriceNet(
                                pricesAndTaxCalculator.changeDoubleToBigDecimal(sellPriceGross),product.getSalesTaxRate());


        product.setSalePriceNet(sellingNetPrice);
        product.setSalePriceGross(pricesAndTaxCalculator.changeDoubleToBigDecimal(sellPriceGross));
        product.setSaleMarginNet(margin);

        return productMapper.mapFromDomainToDto(productsRepository.save(product));
    }

    @NotNull
    private ProductDto getProductDto(final BigDecimal margin, final Product product) {


        var sellPriceNet = pricesAndTaxCalculator.countSellingPriceNetByMargin(margin, product.getPurchaseNetPrice());
        var sellPriceGross = pricesAndTaxCalculator.countPriceGross(sellPriceNet, product.getSalesTaxRate());


        product.setSaleMarginNet(margin);
        product.setSalePriceNet(sellPriceNet);
        product.setSalePriceGross(sellPriceGross);

        return productMapper.mapFromDomainToDto(productsRepository.save(product));
    }

    @NotNull
    private ProductDto getProductDto(final Long productId, final Double sellPriceNet) {
        var sellingPriceNet = pricesAndTaxCalculator.changeDoubleToBigDecimal(sellPriceNet);
        var product = productsRepoProxyService.getProductById(productId);
        var purchasePriceNet = product.getPurchaseNetPrice();
        var margin = pricesAndTaxCalculator.countMarginByPriceNet(purchasePriceNet, sellingPriceNet);

        product.setSalePriceNet(sellingPriceNet);
        product.setSalePriceGross(pricesAndTaxCalculator.countPriceGross(sellingPriceNet, product.getSalesTaxRate()));
        product.setSaleMarginNet(margin);

        return productMapper.mapFromDomainToDto(productsRepository.save(product));
    }

    @NotNull
    private ProductDto getProductDto(final String catalogNum, final Double sellPriceNet) {
        var sellingPriceNet = pricesAndTaxCalculator.changeDoubleToBigDecimal(sellPriceNet);
        var product = productsRepoProxyService.getProductByCatalogNum(catalogNum);
        var margin = pricesAndTaxCalculator.countMarginByPriceNet(product.getPurchaseNetPrice(), sellingPriceNet);

        product.setSalePriceNet(sellingPriceNet);
        product.setSalePriceGross(pricesAndTaxCalculator.countPriceGross(sellingPriceNet, product.getSalesTaxRate()));
        product.setSaleMarginNet(margin);

        return productMapper.mapFromDomainToDto(productsRepository.save(product));
    }

}
