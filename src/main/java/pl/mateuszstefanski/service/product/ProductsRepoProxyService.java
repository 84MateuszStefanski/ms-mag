package pl.mateuszstefanski.service.product;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.dto.mapper.product.ProductMapper;
import pl.mateuszstefanski.exceptions.CannotBeDeletedException;
import pl.mateuszstefanski.exceptions.ProductAlreadyExistsException;
import pl.mateuszstefanski.exceptions.ProductNotFoundException;
import pl.mateuszstefanski.repository.product.ProductsRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductsRepoProxyService {

    private final ProductsRepository productsRepository;
    private final ProductMapper productMapper;

    public List<Long> getIdList(List<SelectedProduct> products){
        return products.stream().map(SelectedProduct::getProductId).collect(Collectors.toList());
    }

    public List<Product> getProductsListFromIdsList(List<Long> idList){
        return idList.stream().map(this::getProductById).collect(Collectors.toList());
    }


    public Product reduceQuantityInStock(String catalogNum, Double quantity){
        var product = getProductByCatalogNum(catalogNum);
        product.setQuantity(product.getQuantity() - quantity);
        return productsRepository.save(product);
    }

    public Product increaseQuantityInStock(String catalogNum, Double quantity){
        var product = getProductByCatalogNum(catalogNum);
        product.setQuantity(product.getQuantity() + quantity);
        return productsRepository.save(product);
    }

    public boolean productAvailable(String catalogNum, Double quantity){
        var product = getProductByCatalogNum(catalogNum);
        return (product.getQuantity() >= quantity);
    }

    List<ProductDto> getAllProducts() {
        return productsRepository.findAll().stream()
                .map(productMapper::mapFromDomainToDto)
                .collect(Collectors.toList());
    }

    void deleteProductById(final Long productId) {
        var product = getProductById(productId);
        if (product.isHasHistory()){
            throw new CannotBeDeletedException();
        }
        productsRepository.delete(product);
    }

    void deleteProductByCatalogNum(final String catalogNum) {
        var product = getProductByCatalogNum(catalogNum);
        if (product.isHasHistory()){
            throw new CannotBeDeletedException();
        }
        productsRepository.delete(product);
    }

    void checkIfProductNotExistsInDatabase(Product product){
        var productToCheck = productsRepository.findProductByProductId(product.getProductId());
        if (productToCheck.isPresent()){
            throw new ProductAlreadyExistsException(product.getProductId());
        }
    }

    BigDecimal getProductSaleTaxRate(Long productId){
        return getProductById(productId).getSalesTaxRate().getTAX_VALUE();
    }

    BigDecimal getProductPurchaseTaxRate(Long productId){
        return getProductById(productId).getPurchaseTaxRate().getTAX_VALUE();
    }

    BigDecimal getProductPurchasePriceNet(Long productId){
        return getProductById(productId).getPurchaseNetPrice();
    }

    public Long getCatalogNumReturnProductId(String catalogNum){
        var product = getProductByCatalogNum(catalogNum);
        return product.getProductId();
    }

    public void setProtectionAgainstDeletionForAllNewList(List<SelectedProduct> list){
        list.stream().forEach(prod -> preventRemovalOfProduct(prod.getProductId()));
    }


    public void preventRemovalOfProduct(Long productId){
        var product = getProductById(productId);
        if(!product.isHasHistory()) {
            product.setHasHistory(true);
            productsRepository.save(product);
        }
    }

    private void preventRemovalOfProduct(String catalogNum){
        var product = getProductByCatalogNum(catalogNum);
        if(!product.isHasHistory()) {
            product.setHasHistory(true);
            productsRepository.save(product);
        }
    }


    public Double getProductQuantityByCatalogNum(String catalogNum){
        return getProductByCatalogNum(catalogNum).getQuantity();
    }

    public Product getProductById(Long productId){
        var product = productsRepository.findProductByProductId(productId);
        if (product.isEmpty()){
            throw new ProductNotFoundException(productId);
        }
        return product.get();
    }

    public Product getProductByCatalogNum(String catalogNum){
        var product = productsRepository.findProductByCatalogNumber(catalogNum);
        if (product.isEmpty()){
            throw new ProductNotFoundException(catalogNum);
        }
        return product.get();
    }

    Product updateProduct(Product oldData, ProductDto newData) {

        oldData.setName(newData.getName());
        oldData.setCatalogNumber(newData.getCatalogNumber());
        oldData.setCategory(newData.getCategory());
        oldData.setEanCode(newData.getEanCode());
        oldData.setPurchaseNetPrice(newData.getPurchaseNetPrice());
        oldData.setPurchaseTaxRate(newData.getPurchaseTaxRate());
        oldData.setStockUnit(newData.getStockUnit());
        oldData.setTypeOfArticle(newData.getTypeOfArticle());

        return oldData;

    }

   Product saveProduct(Product product){
        return productsRepository.save(product);
    }
















}
