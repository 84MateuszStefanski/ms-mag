package pl.mateuszstefanski.service.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.dto.mapper.product.ProductMapper;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.service.DomainServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ProductsService implements DomainServiceInterface<ProductDto> {


    private final ProductMapper productMapper;
    private final ProductsRepoProxyService productsRepoProxyService;


    @Override
    public ProductDto addNew(final ProductDto productDto) {
        var product = productMapper.mapFromDtoToDomain(productDto);
        productsRepoProxyService.checkIfProductNotExistsInDatabase(product);
        product.setHasHistory(false);
        productsRepoProxyService.saveProduct(product);
        return productDto;
    }

    @Override
    public ProductDto getById(final Long id) {
        var product = productsRepoProxyService.getProductById(id);
        return productMapper.mapFromDomainToDto(product);
    }

    @Override
    public ProductDto getByNum(final String num) {
        var product = productsRepoProxyService.getProductByCatalogNum(num);
        return productMapper.mapFromDomainToDto(product);
    }

    @Override
    public List<ProductDto> getAll() {
        return productsRepoProxyService.getAllProducts();
    }

    @Override
    public ProductDto updateById(final Long id, final ProductDto productDto) {
        var beforeUpdate = productsRepoProxyService.getProductById(id);
        var afterUpdate = productsRepoProxyService.updateProduct(beforeUpdate, productDto);

        return productMapper.mapFromDomainToDto(productsRepoProxyService.saveProduct(afterUpdate));
    }

    @Override
    public ProductDto updateByNum(final String num, final ProductDto productDto) {
        var beforeUpdate = productsRepoProxyService.getProductByCatalogNum(num);
        var afterUpdate = productsRepoProxyService.updateProduct(beforeUpdate,productDto);

        return productMapper.mapFromDomainToDto(productsRepoProxyService.saveProduct(afterUpdate));
    }

    @Override
    public void deleteById(final Long id) {
        productsRepoProxyService.deleteProductById(id);
    }

    @Override
    public void deleteByNum(final String num) {
        productsRepoProxyService.deleteProductByCatalogNum(num);
    }



}
