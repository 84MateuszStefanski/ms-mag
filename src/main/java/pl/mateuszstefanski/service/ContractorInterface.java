package pl.mateuszstefanski.service;

public interface ContractorInterface<T> {

    T addContractor(String taxIdNum);
}
