package pl.mateuszstefanski.service;

import java.util.List;

public interface DomainServiceInterface <T>{

    T addNew(T t);

    T getById(Long id);

    T getByNum(String num);

    List<T> getAll();

    T updateById(Long id, T t);

    T updateByNum(String num, T t);

    void deleteById(Long id);

    void deleteByNum(String num);
}
