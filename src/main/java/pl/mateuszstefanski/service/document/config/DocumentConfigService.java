package pl.mateuszstefanski.service.document.config;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.document.PlaceOfIssue;
import pl.mateuszstefanski.dto.document.PlaceOfIssueDto;
import pl.mateuszstefanski.dto.mapper.document.PlaceOfIssueMapper;
import pl.mateuszstefanski.repository.document.PlaceOfIssueRepository;

@RequiredArgsConstructor
@Service
public class DocumentConfigService {

    private final PlaceOfIssueRepository repository;
    private final PlaceOfIssueMapper mapper;

    public PlaceOfIssueDto setPlaceOfDocumentIssue(final String placeOfIssue) {
        cleanDatabase();
        var place = mapper.mapFromDtoToDomain(PlaceOfIssueDto.builder().placeOfIssue(placeOfIssue).build());
        return saveAndMapToDto(place);
    }

    public PlaceOfIssueDto getPlaceOfDocumentIssue() {
        var placeOptional = repository.findAll().stream().findFirst();
        if (placeOptional.isPresent()) {
            return mapper.mapFromDomainToDto(placeOptional.get());
        } else {
            return PlaceOfIssueDto.builder().placeOfIssue("").build();
        }
    }

    private PlaceOfIssueDto saveAndMapToDto(PlaceOfIssue place){
        return mapper.mapFromDomainToDto(repository.save(place));
    }

    private void cleanDatabase() {
        var list = repository.findAll();
        if (!list.isEmpty()) {
            repository.deleteAll();
        }

    }
}
