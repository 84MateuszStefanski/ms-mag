package pl.mateuszstefanski.service.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.document.PurchaseInvoice;
import pl.mateuszstefanski.domain.document.SalesInvoice;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.repository.document.SalesInvoicesRepository;
import pl.mateuszstefanski.service.prices.PricesAndTaxCalculator;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;

import java.time.LocalDate;
import java.util.List;
import java.util.OptionalLong;

@RequiredArgsConstructor
@Service
public class SalesInvoiceRepoProxyService {

    private final SalesInvoicesRepository invoicesRepository;

    private final ProductsRepoProxyService productsRepoProxyService;

    private final PricesAndTaxCalculator calculator;

    String getInvoiceNum() {

        OptionalLong lastId;
        lastId = invoicesRepository.findAll().stream().mapToLong(SalesInvoice::getInvoiceId).max();
        if (lastId.isEmpty() || lastId.getAsLong() == 0) {
            return "F/" + 1 + "/" + LocalDate.now().getYear();
        }
        return "F/" + (lastId.getAsLong() + 1L) + "/" + LocalDate.now().getYear();
    }

    public SalesInvoice saveInvoice(final SalesInvoice invoice) {
        return invoicesRepository.save(invoice);
    }

    public void recalculateReceiptValues(SalesInvoice invoice , List<SelectedProduct> products){
        var idsList = productsRepoProxyService.getIdList(products);
        var netTotal = calculator.calculateNewAmountNet(products);
        invoice.setTotalSumNet(netTotal);
        invoice.setTotalSumGross(calculator.calculateNewAmountGross(products));
        invoice.setTotalTax(calculator.countTaxValue(invoice.getTotalSumGross(), invoice.getTotalSumNet()));
    }

}
