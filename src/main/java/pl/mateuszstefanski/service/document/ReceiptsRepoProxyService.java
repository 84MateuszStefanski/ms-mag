package pl.mateuszstefanski.service.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.document.Receipt;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.repository.document.ReceiptsRepository;
import pl.mateuszstefanski.service.prices.PricesAndTaxCalculator;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class ReceiptsRepoProxyService {

    private final ProductsRepoProxyService productsRepoProxyService;
    private final PricesAndTaxCalculator calculator;
    private final ReceiptsRepository receiptsRepository;



    public void recalculateReceiptValues(Receipt receipt , List<SelectedProduct> products){
        var idsList = productsRepoProxyService.getIdList(products);
        var prodList = productsRepoProxyService.getProductsListFromIdsList(idsList);
        var netTotal = calculator.calculateNewAmountNet(products);
        receipt.setTotalSumNet(netTotal);
        receipt.setTotalSumGross(calculator.calculateNewAmountGross(products));
        receipt.setTotalTax(calculator.countTaxValue(receipt.getTotalSumGross(), receipt.getTotalSumNet()));
    }

    String getNewReceiptNum() {
        var lastId = receiptsRepository.findAll().stream().mapToLong(Receipt::getNewReceiptId).max();
        if (lastId.isEmpty() || lastId.getAsLong() == 0) {
            return "R/" + 1 + "/" + LocalDate.now().getYear();
        }
        return "R/" + (lastId.getAsLong() + 1L) + "/" + LocalDate.now().getYear();
    }

    Receipt saveReceipt(Receipt receipt){
        return receiptsRepository.save(receipt);
    }


    List<Receipt> getAll() {
        return receiptsRepository.findAll();
    }
}
