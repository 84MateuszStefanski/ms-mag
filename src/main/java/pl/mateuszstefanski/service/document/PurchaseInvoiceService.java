package pl.mateuszstefanski.service.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.mateuszstefanski.domain.document.PurchaseInvoice;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.document.PurchaseInvoiceDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.dto.mapper.document.PurchaseInvoiceManualMapper;
import pl.mateuszstefanski.dto.mapper.document.SelectedProductManualMapper;
import pl.mateuszstefanski.service.ContractorInterface;
import pl.mateuszstefanski.service.PurchaseDocumentInterface;
import pl.mateuszstefanski.service.customer.CustomersService;
import pl.mateuszstefanski.service.document.config.DocumentConfigService;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;
import pl.mateuszstefanski.service.user.UsersRepoProxyService;
import pl.mateuszstefanski.webservice.service.CurrencyConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SessionScope
@RequiredArgsConstructor
@Service
public class PurchaseInvoiceService implements
        PurchaseDocumentInterface<PurchaseInvoiceDto>, ContractorInterface<PurchaseInvoiceDto> {

    private PurchaseInvoice invoice;
    private List<SelectedProduct> products = new ArrayList<>();
    private final UsersRepoProxyService usersRepoProxyService;
    private final ProductsRepoProxyService productsRepoProxyService;
    private final PurchaseInvoiceRepoProxyService invoiceRepoProxyService;
    private final DocumentConfigService documentConfigService;
    private final CustomersService customersService;
    private final PurchaseInvoiceManualMapper mapper;
    private final SelectedProductManualMapper selectedProductManualMapper;
    private final CustomerMapper customerMapper;

    private final CurrencyConverter converter;



    @Override
    public PurchaseInvoiceDto createDocument(final String number) {
        this.invoice = PurchaseInvoice.builder()
                .invoiceNumber(number)
                .dateOfIssue(LocalDate.now())
                .placeOfIssue(documentConfigService.getPlaceOfDocumentIssue().getPlaceOfIssue())
                .buyerId(usersRepoProxyService.getLoggedUser().getUserId())
                .products(this.products)
                .totalSumNet(BigDecimal.ZERO)
                .totalSumGross(BigDecimal.ZERO)
                .totalTax(BigDecimal.ZERO)
                .build();
        var invoiceDto = mapper.mapFromDomainToDtoForIssue(this.invoice);
        invoiceDto.setTotalSumGrossInEuro(0D);
        return invoiceDto;
    }

    @Override
    public PurchaseInvoiceDto addContractor(final String taxIdNum) {
        var sellerDto = customersService.getByNum(taxIdNum);
        var seller = customerMapper.mapFromDtoToDomain(sellerDto);
        this.invoice.setSellerId(seller.getCustomerId());
        var invoiceDto = mapper.mapFromDomainToDto(this.invoice);
        invoiceDto.setSeller(sellerDto);
        customersService.preventRemovalOfCustomer(taxIdNum);
        return invoiceDto;
    }

    @Override
    public PurchaseInvoiceDto addProductToDocument(final String catalogNum, final Double quantity) {
        var selectedProduct = selectedProductManualMapper.mapFromProductToSelectedProductForPurchase(catalogNum, quantity);

        this.products.add(selectedProduct);

        invoiceRepoProxyService.recalculateReceiptValues(getInvoice(), getProductsList());
        productsRepoProxyService.increaseQuantityInStock(catalogNum, quantity);
        var invoiceDto = mapper.mapFromDomainToDto(getInvoice());
        invoiceDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(invoiceDto.getTotalSumGross()));
        return invoiceDto;
    }

    @Override
    public PurchaseInvoiceDto acceptDocument() {
        this.invoice.setProducts(this.getProductsList());
        productsRepoProxyService.setProtectionAgainstDeletionForAllNewList(getProductsList());
        var invoiceDto = mapper.mapFromDomainToDto(invoiceRepoProxyService.saveInvoice(this.invoice));
        invoiceDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(invoiceDto.getTotalSumGross()));
        return invoiceDto;
    }

    public List<SelectedProduct> getProductsList() {
        return Collections.unmodifiableList(products);
    }

    PurchaseInvoice getInvoice() {
        return this.invoice;
    }
}
