package pl.mateuszstefanski.service.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.document.PurchaseInvoice;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.repository.document.PurchaseInvoicesRepository;
import pl.mateuszstefanski.service.prices.PricesAndTaxCalculator;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class PurchaseInvoiceRepoProxyService {

    private final ProductsRepoProxyService productsRepoProxyService;

    private final PricesAndTaxCalculator calculator;

    private final PurchaseInvoicesRepository invoicesRepository;


    public PurchaseInvoice saveInvoice(final PurchaseInvoice invoice) {
        return invoicesRepository.save(invoice);
    }

    public void recalculateReceiptValues(PurchaseInvoice invoice , List<SelectedProduct> products){
        var idsList = productsRepoProxyService.getIdList(products);
        var netTotal = calculator.calculateNewAmountNet(products);
        invoice.setTotalSumNet(netTotal);
        invoice.setTotalSumGross(calculator.calculateNewAmountGross(products));
        invoice.setTotalTax(calculator.countTaxValue(invoice.getTotalSumGross(), invoice.getTotalSumNet()));
    }

}
