package pl.mateuszstefanski.service.document;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.mateuszstefanski.domain.document.SalesInvoice;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.document.SalesInvoiceDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.dto.mapper.document.SalesInvoiceManualMapper;
import pl.mateuszstefanski.dto.mapper.document.SelectedProductManualMapper;
import pl.mateuszstefanski.service.ContractorInterface;
import pl.mateuszstefanski.service.SellingDocumentInterface;
import pl.mateuszstefanski.service.customer.CustomersService;
import pl.mateuszstefanski.service.document.config.DocumentConfigService;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;
import pl.mateuszstefanski.service.user.UsersRepoProxyService;
import pl.mateuszstefanski.webservice.service.CurrencyConverter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@SessionScope
@RequiredArgsConstructor
@Service
public class SalesInvoiceService implements
        SellingDocumentInterface<SalesInvoiceDto>, ContractorInterface<SalesInvoiceDto> {

    private SalesInvoice invoice;
    private List<SelectedProduct> products = new ArrayList<>();

    private final DocumentConfigService documentConfigService;

    private final UsersRepoProxyService usersRepoProxyService;

    private final SalesInvoiceRepoProxyService salesInvoiceRepoProxyService;

    private final SalesInvoiceManualMapper salesInvoiceManualMapper;

    private final CustomerMapper customerMapper;

    private final SelectedProductManualMapper selectedProductManualMapper;

    private final CustomersService customersService;
    private final ProductsRepoProxyService productsRepoProxyService;

    private final CurrencyConverter converter;


    @Override
    public SalesInvoiceDto createDocument() {
        this.invoice = SalesInvoice.builder()
                .invoiceNumber(salesInvoiceRepoProxyService.getInvoiceNum())
                .dateOfIssue(LocalDate.now())
                .placeOfIssue(documentConfigService.getPlaceOfDocumentIssue().getPlaceOfIssue())
                .sellerId(usersRepoProxyService.getLoggedUser().getUserId())
                .products(this.products)
                .totalSumNet(BigDecimal.ZERO)
                .totalSumGross(BigDecimal.ZERO)
                .totalTax(BigDecimal.ZERO)
                .build();

        var invoiceDto = salesInvoiceManualMapper.mapFromDomainToDtoForIssue(this.invoice);
        invoiceDto.setTotalSumGrossInEuro(0D);
        return invoiceDto;
    }

    @Override
    public SalesInvoiceDto addContractor(final String taxIdNum) {
        var buyerDto = customersService.getByNum(taxIdNum);
        var buyer = customerMapper.mapFromDtoToDomain(buyerDto);
        this.invoice.setBuyerId(buyer.getCustomerId());
        var invoiceDto = salesInvoiceManualMapper.mapFromDomainToDto(this.invoice);
        invoiceDto.setBuyer(buyerDto);
        customersService.preventRemovalOfCustomer(taxIdNum);
        return invoiceDto;
    }

    @Override
    public SalesInvoiceDto addProductToDocument(final String catalogNum, final Double quantity) {
        var selectedProduct = selectedProductManualMapper.mapFromProductToSelectedProductForSell(catalogNum, quantity);
        this.products.add(selectedProduct);
        salesInvoiceRepoProxyService.recalculateReceiptValues(getInvoice(), getProducts());
        productsRepoProxyService.reduceQuantityInStock(catalogNum,quantity);

        var invoiceDto = salesInvoiceManualMapper.mapFromDomainToDto(getInvoice());
        invoiceDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(invoiceDto.getTotalSumGross()));
        return invoiceDto;
    }

    @Override
    public SalesInvoiceDto acceptDocument() {
        this.invoice.setProducts(this.getProducts());
        productsRepoProxyService.setProtectionAgainstDeletionForAllNewList(getProducts());
        var invoiceDto = salesInvoiceManualMapper.mapFromDomainToDto(salesInvoiceRepoProxyService.saveInvoice(this.invoice));
        invoiceDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(invoiceDto.getTotalSumGross()));
        return invoiceDto;
    }



    SalesInvoice getInvoice() {
        return invoice;
    }

    List<SelectedProduct> getProducts() {
        return Collections.unmodifiableList(products);
    }

    public void export(final HttpServletResponse response) throws IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        fontTitle.setSize(18);

        Paragraph invoiceNumber = new Paragraph(this.invoice.getInvoiceNumber(), fontTitle);
        invoiceNumber.setAlignment(Paragraph.ALIGN_CENTER);

        Font fontParagraph = FontFactory.getFont(FontFactory.HELVETICA);
        fontParagraph.setSize(12);

        Paragraph placeOfIssue = new Paragraph(this.invoice.getPlaceOfIssue(), fontParagraph);
        placeOfIssue.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph dateOfIssue = new Paragraph(this.invoice.getDateOfIssue().toString(), fontParagraph);
        dateOfIssue.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph user = new Paragraph("Seller" + "\n" + usersRepoProxyService.getLoggedUser().toString(), fontParagraph);
        user.setAlignment(Paragraph.ALIGN_LEFT);
        var client = customersService.getById(this.invoice.getBuyerId()).toString();
        Paragraph customer = new Paragraph(client, fontParagraph);
        customer.setAlignment(Paragraph.ALIGN_RIGHT);
        Paragraph sumGross = new Paragraph("Sum gross = " + this.invoice.getTotalSumGross().toString(), fontParagraph);

        document.add(invoiceNumber);
        document.add(placeOfIssue);
        document.add(dateOfIssue);
        document.add(user);
        document.add(customer);
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));

        for (SelectedProduct selectedProduct : this.products) {
            var p = new Paragraph(selectedProductManualMapper.mapFromDomainToDto(selectedProduct).toString() + "\n", fontParagraph);
            var emptyLine = new Paragraph("----------------------------------------------");
            document.add(p);
            document.add(emptyLine);
        }

        document.add(sumGross);
        document.close();
    }
}
