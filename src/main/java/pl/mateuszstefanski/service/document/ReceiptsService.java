package pl.mateuszstefanski.service.document;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.mateuszstefanski.domain.document.Receipt;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.dto.document.ReceiptDto;
import pl.mateuszstefanski.dto.mapper.document.NewReceiptManualMapper;
import pl.mateuszstefanski.dto.mapper.document.SelectedProductManualMapper;
import pl.mateuszstefanski.service.SellingDocumentInterface;
import pl.mateuszstefanski.service.document.config.DocumentConfigService;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;
import pl.mateuszstefanski.service.user.UsersRepoProxyService;
import pl.mateuszstefanski.webservice.service.CurrencyConverter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@SessionScope
@RequiredArgsConstructor
@Service
public class ReceiptsService implements SellingDocumentInterface<ReceiptDto> {
    private Receipt receipt;
    private List<SelectedProduct> products = new ArrayList<>();
    private final DocumentConfigService documentConfigService;
    private final NewReceiptManualMapper mapper;

    private final SelectedProductManualMapper selectedProductManualMapper;
    private final ReceiptsRepoProxyService repoProxyService;

    private final ProductsRepoProxyService productsRepoProxyService;

    private final CurrencyConverter converter;

    private final UsersRepoProxyService usersRepoProxyService;


    @Override
    public ReceiptDto createDocument() {
        receipt = Receipt.builder()
                .receiptNumber(repoProxyService.getNewReceiptNum())
                .dateOfIssue(LocalDateTime.now())
                .placeOfIssue(documentConfigService.getPlaceOfDocumentIssue().getPlaceOfIssue())
                .products(this.products)
                .totalSumNet(BigDecimal.ZERO)
                .totalSumGross(BigDecimal.ZERO)
                .totalTax(BigDecimal.ZERO)
                .build();

        var receiptDto = mapper.mapFromDomainToDto(receipt);
        receiptDto.setTotalSumGrossInEuro(0D);

        return receiptDto;
    }

    @Override
    public ReceiptDto addProductToDocument(final String catalogNum, final Double quantity) {
        var selectedProduct = selectedProductManualMapper.mapFromProductToSelectedProductForSell(catalogNum, quantity);

        this.products.add(selectedProduct);

        repoProxyService.recalculateReceiptValues(getReceipt(), getProductsList());
        productsRepoProxyService.reduceQuantityInStock(catalogNum, quantity);
        var receiptDto = mapper.mapFromDomainToDto(getReceipt());
        receiptDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(receiptDto.getTotalSumGross()));
        return receiptDto;
    }

    @Override
    public ReceiptDto acceptDocument() {
        this.getReceipt().setProducts(this.getProductsList());
        productsRepoProxyService.setProtectionAgainstDeletionForAllNewList(getProductsList());
        var receiptDto = mapper.mapFromDomainToDto(repoProxyService.saveReceipt(this.getReceipt()));
        receiptDto.setTotalSumGrossInEuro(converter.convertPlnToEuro(receiptDto.getTotalSumGross()));
        return receiptDto;
    }

    public List<SelectedProduct> getProductsList() {
        return Collections.unmodifiableList(products);
    }

    Receipt getReceipt() {
        return this.receipt;
    }

    public List<ReceiptDto> getAllReceipts() {
        return repoProxyService.getAll().stream().map(mapper::mapFromDomainToDto).collect(Collectors.toList());
    }

    public void export(final HttpServletResponse response) throws IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font fontTitle = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        fontTitle.setSize(18);

        Paragraph receiptNumber = new Paragraph(this.receipt.getReceiptNumber(), fontTitle);
        receiptNumber.setAlignment(Paragraph.ALIGN_CENTER);

        Font fontParagraph = FontFactory.getFont(FontFactory.HELVETICA);
        fontParagraph.setSize(12);

        Paragraph placeOfIssue = new Paragraph(this.receipt.getPlaceOfIssue(), fontParagraph);
        placeOfIssue.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph dateOfIssue = new Paragraph(this.receipt.getDateOfIssue().toLocalDate().toString(), fontParagraph);
        dateOfIssue.setAlignment(Paragraph.ALIGN_RIGHT);

        Paragraph user = new Paragraph(usersRepoProxyService.getLoggedUser().toString(), fontParagraph);

        Paragraph sumGross = new Paragraph("Sum gross = " + this.receipt.getTotalSumGross().toString(), fontParagraph);

        document.add(receiptNumber);
        document.add(placeOfIssue);
        document.add(dateOfIssue);
        document.add(user);
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));

        for (SelectedProduct selectedProduct : this.products) {
            var p = new Paragraph(selectedProductManualMapper.mapFromDomainToDto(selectedProduct).toString() + "\n", fontParagraph);
            var emptyLine = new Paragraph("----------------------------------------------");
            document.add(p);
            document.add(emptyLine);
        }

        document.add(sumGross);
        document.close();
    }
}
