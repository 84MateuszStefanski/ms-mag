package pl.mateuszstefanski.service;

public interface PurchaseDocumentInterface<T>{

    T createDocument(String number);
    T addProductToDocument(String catalogNum, Double quantity);
    T acceptDocument();
}
