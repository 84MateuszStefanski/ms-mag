package pl.mateuszstefanski.service.prices;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.service.product.ProductsRepoProxyService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


@RequiredArgsConstructor
@Service
public class PricesAndTaxCalculator {

    private final ProductsRepoProxyService service;
    private final ProductsRepoProxyService productsRepoProxyService;


    /**
     * Calculates the total gross purchases for the entire list
     */
    public BigDecimal calculateNewAmountGross(List<SelectedProduct> products) {

        if (products == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }

        var amountGross = BigDecimal.ZERO;

        if (products.isEmpty()){
            return amountGross;
        }

        for (SelectedProduct product : products) {
            var tax = service.getProductById(product.getProductId()).getSalesTaxRate();
            var net = product.getSumNet();
            var price = countPriceGross(net, tax);
            if (price == null){
                price = BigDecimal.ZERO;
            }
            amountGross = amountGross.add(price);
        }
        return amountGross;
    }

    public BigDecimal countSellingPriceGrossByMargin(final BigDecimal margin, final BigDecimal purchasePriceNet, final Tax tax) {
        if (margin == null || purchasePriceNet == null || tax == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (margin.equals(BigDecimal.ZERO)) {
            return purchasePriceNet
                    .multiply(tax.getTAX_VALUE())
                    .setScale(2, RoundingMode.HALF_UP);
        }
        return countSellingPriceNetByMargin(margin, purchasePriceNet)
                .multiply(tax.getTAX_VALUE())
                .setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Consumes margin in 1.25 format as 25%
     */
    public BigDecimal countSellingPriceNetByMargin(final BigDecimal margin, final BigDecimal purchasePriceNet) {
        if (margin == null || purchasePriceNet == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (margin.equals(BigDecimal.ZERO)) {
            return purchasePriceNet;
        }
        return purchasePriceNet.multiply(margin)
                .setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Returning margin in format 70 as 70%
     */
    public BigDecimal countMarginByPriceGross(final BigDecimal purchasePriceNet, final BigDecimal sellPriceGross) {
        if (purchasePriceNet == null || sellPriceGross == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (countPriceNet(sellPriceGross, Tax.TAX23).equals(purchasePriceNet)) {
            return BigDecimal.ZERO;
        }
        return countMarginByPriceNet
                (purchasePriceNet, sellPriceGross.divide(Tax.TAX23.getTAX_VALUE(), 2, RoundingMode.HALF_UP));
    }

    /**
     * Returning margin in format 70 as 70%
     */
    public BigDecimal countMarginByPriceNet(final BigDecimal purchasePriceNet, final BigDecimal sellPriceNet) {
        if (purchasePriceNet == null || sellPriceNet == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (purchasePriceNet.equals(BigDecimal.ZERO) || sellPriceNet.equals(purchasePriceNet)) {
            return BigDecimal.ZERO;
        }
        return (sellPriceNet
                .divide(purchasePriceNet, 3, RoundingMode.HALF_UP))
                .subtract(BigDecimal.ONE)
                .multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal countPriceGross(final BigDecimal priceNet, Tax tax) {
        if (priceNet == null || tax == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (priceNet.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return priceNet.multiply(tax.getTAX_VALUE())
                .setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal countPriceNet(final BigDecimal priceGross, Tax tax) {
        if (priceGross == null || tax == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (priceGross.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        if (tax.getTAX_VALUE().equals(BigDecimal.ZERO)) {
            return priceGross;
        }

        return priceGross.divide(tax.getTAX_VALUE(), 2, RoundingMode.HALF_UP);
    }

    public BigDecimal countTaxValue(final BigDecimal priceGross, final BigDecimal priceNet) {
        if (priceGross == null || priceNet == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (priceGross.equals(BigDecimal.ZERO) || priceNet.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return priceGross.subtract(priceNet).setScale(2, RoundingMode.HALF_UP);
    }

    /**
     * Returning tax percent  i 1,XX format
     */
    BigDecimal countTaxPercent(final BigDecimal priceGross, final BigDecimal priceNet) {
        if (priceGross == null || priceNet == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (priceGross.equals(BigDecimal.ZERO) || priceNet.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return priceGross.divide(priceNet, 2, RoundingMode.HALF_UP);
    }

    public BigDecimal changeDoubleToBigDecimal(Double number) {

        if (number == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }


        return BigDecimal.valueOf(number).setScale(2, RoundingMode.HALF_UP);
    }

    public Double changeBigDecimalToDouble(BigDecimal number) {
        if (number == null){
            throw new IllegalArgumentException("Number can`t be null");
        }
        return number.doubleValue();
    }

    /**
     * consuming 20,0 double and returning 1,2 decimal
     */
    public BigDecimal changeMarginFormatDoubleToDecimal(Double margin) {
        if (margin == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (margin == 0) {
            return BigDecimal.ZERO;
        }
        return (BigDecimal.valueOf(margin).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP)).add(BigDecimal.ONE);
    }

    public Double changeMarginFormatDecimalToDouble(BigDecimal margin) {
        if (margin == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }
        if (margin.equals(BigDecimal.ZERO)) {
            return 0D;
        }
        return (margin.subtract(BigDecimal.ONE)).multiply(BigDecimal.valueOf(100)).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Calculates the total net purchases for the entire list
     */
    public BigDecimal calculateNewAmountNet(List<SelectedProduct> selectedProducts) {

        if (selectedProducts == null){
            throw new IllegalArgumentException("Argument can`t be null");
        }

        var amountNet = BigDecimal.ZERO;

        if (selectedProducts.isEmpty()){
            return amountNet;
        }

        for (SelectedProduct product : selectedProducts) {
            var price = product.getSumNet();
            if (price == null){
                price = BigDecimal.ZERO;
            }
            amountNet = amountNet.add(price);
        }
        return amountNet;

    }




}

