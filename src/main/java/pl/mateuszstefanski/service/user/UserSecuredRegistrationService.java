package pl.mateuszstefanski.service.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.dto.user.UserDto;
import pl.mateuszstefanski.dto.user.UserRegistrationDto;
import pl.mateuszstefanski.dto.mapper.user.UsersMapper;

import java.util.Arrays;
import java.util.List;

@SessionScope
@Service
@RequiredArgsConstructor
public class UserSecuredRegistrationService {

    private final UsersMapper usersMapper;
    private final PasswordEncoder passwordEncoder;
    private final UsersRepoProxyService usersRepoProxyService;



    public UserRegistrationDto registerUser(final UserRegistrationDto userDto) {

          var user = User.builder()
                .login(userDto.getLogin())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .authorities(userAuthorities())
                .build();

        return usersMapper.mapFromDomainToRegistrationDto(usersRepoProxyService.saveUser(user));
    }

    public UserDto addData(final String login, final UserDto userDto) {
        var user = usersRepoProxyService.getUser(login);
            user.setName(userDto.getName());
            user.setTaxIdentityNr(userDto.getTaxIdentityNr());
            user.setPostCode(userDto.getPostCode());
            user.setCity(userDto.getCity());
            user.setStreet(userDto.getStreet());
            user.setPhoneNumber(userDto.getPhoneNumber());
            user.setEmail(userDto.getEmail());
            user.setBankAccountNumber(userDto.getBankAccountNumber());

        return usersMapper.mapFromDomainToDto(usersRepoProxyService.saveUser(user));
    }


    private List<String> userAuthorities(){
        return Arrays.asList(
                "users:read","users:update","users:remove",
                "customers:read","customers:write","customers:update","customers:remove",
                "receipts:read","receipts:write","receipts:update","receipts:remove",
                "products:read","products:write","products:update", "products:remove",
                "invoices:read","invoices:write","invoices:update",
                "doc-config:read","doc-config:write",
                "pdf:read");
    }



}


