package pl.mateuszstefanski.service.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.user.User;
import pl.mateuszstefanski.repository.user.UsersRepository;
import pl.mateuszstefanski.security.SecuredUserDetailsService;

import javax.validation.constraints.NotNull;
import java.util.List;

@RequiredArgsConstructor
@Service
public class UsersRepoProxyService {

    private final SecuredUserDetailsService securedUserDetailsService;
    private final UsersRepository usersRepository;


    public User getUserFromListById(Long userId){
      var optionalUser =   getAllUsers().stream().filter(user -> user.getUserId() == userId).findFirst();
      if (optionalUser.isEmpty()){
          throw new UsernameNotFoundException("User with id " + userId);
      }
      return optionalUser.get();
    }

    private List<User> getAllUsers(){
        return usersRepository.findAll();
    }

    public User getUserById(Long userId){
        var userOptional = usersRepository.findByUserId(userId);

        if (userOptional.isEmpty()){
            throw new UsernameNotFoundException("User with id " + userId);
        }
        return userOptional.get();
    }

    public User getLoggedUser(){
        var login = getUserLogin();
        var userDetails = securedUserDetailsService.loadUserByUsername(login);
        if (userDetails == null){
            throw new UsernameNotFoundException(login);
        }
        var user = usersRepository.findUserByLogin(userDetails.getUsername());
        return user.get();
    }

    User getUser(String login){
        var userOptional = usersRepository.findUserByLogin(login);
        if (userOptional.isEmpty()){
            throw new UsernameNotFoundException("User not found");
        }
        return userOptional.get();
    }

    private String getUserLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication.getName();
        }else {
            throw new UsernameNotFoundException("User not found");
        }
    }


    User saveUser(@NotNull User user){
       return usersRepository.save(user);
    }
}
