package pl.mateuszstefanski.service.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.customer.Customer;
import pl.mateuszstefanski.dto.customer.CustomerDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.exceptions.CannotBeDeletedException;
import pl.mateuszstefanski.exceptions.CustomerAlreadyExistsException;
import pl.mateuszstefanski.exceptions.CustomerNotFoundException;
import pl.mateuszstefanski.repository.customer.CustomersRepository;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomersRepoProxyService {

    private final CustomersRepository customersRepository;
    private final CustomerMapper customerMapper;

    public void preventRemovalOfCustomer(String taxIdNum){
        var customer = getCustomerByTaxNum(taxIdNum);
        customer.setHasHistory(true);
        customersRepository.save(customer);
    }
    void deleteCustomer(final Long customerId) {
        var customer = getCustomerById(customerId);
        if (customer.isHasHistory()){
            throw new CannotBeDeletedException();
        }
        customersRepository.deleteById(customerId);
    }

    void deleteCustomerByTaxNum(final String taxNum) {
        var customer = getCustomerByTaxNum(taxNum);
        if (customer.isHasHistory()){
            throw new CannotBeDeletedException();
        }
        customersRepository.delete(customer);
    }

    void checkIfCustomerNotExistsInDatabase(final Customer customer){
        var customerToCheck =  customersRepository.findCustomerByCustomerId(customer.getCustomerId());
        if (customerToCheck.isPresent()){
            throw new CustomerAlreadyExistsException(customer.getCustomerId());
        }
    }

    public Customer getCustomerById(final Long customerId){

        var customer = customersRepository.findCustomerByCustomerId(customerId);
        if (customer.isEmpty()){
            throw new CustomerNotFoundException(customerId);
        }
        return customer.get();
    }

    public Customer getCustomerByTaxNum(final String taxNum){
        var customer = customersRepository.findCustomerByTaxIdentityNr(taxNum);
        if (customer.isEmpty()){
            throw new CustomerNotFoundException(taxNum);
        }
        return customer.get();
    }
    public Customer getCustomerFromList(Long customerId){
        var optionalCustomer = getAll().stream().filter(customer -> customer.getCustomerId() == customerId).findFirst();
        if (optionalCustomer.isEmpty()){
            throw new CustomerNotFoundException(customerId);
        }
        return optionalCustomer.get();
    }

    private List<Customer> getAll(){
        return customersRepository.findAll();
    }
    public List<CustomerDto> getAllCustomers() {
        return customersRepository.findAll().stream()
                .map(customerMapper::mapFromDomainToDto)
                .collect(Collectors.toList());
    }

    public Customer updateData(Customer oldData, CustomerDto newData){
        oldData.setName(newData.getName());
        oldData.setCustomerType(newData.getCustomerType());
        oldData.setTaxIdentityNr(newData.getTaxIdentityNr());
        oldData.setCountry(newData.getCountry());
        oldData.setPostCode(newData.getPostCode());
        oldData.setCity(newData.getCity());
        oldData.setStreet(newData.getStreet());
        oldData.setPhone(newData.getPhone());
        oldData.setEmail(newData.getEmail());
        oldData.setSaleLock(newData.isSaleLock());
        oldData.setPermanentDiscount(BigDecimal.valueOf(newData.getPermanentDiscount()));
        oldData.setDaysToPaymentDeadline(newData.getDaysToPaymentDeadline());
        oldData.setBankAccountNumber(newData.getBankAccountNumber());
        return saveCustomer(oldData);
    }

    public Customer saveCustomer(@NotNull Customer customer){
        return customersRepository.save(customer);
    }






}
