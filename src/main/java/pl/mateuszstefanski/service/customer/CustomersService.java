package pl.mateuszstefanski.service.customer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mateuszstefanski.domain.customer.Customer;
import pl.mateuszstefanski.dto.customer.CustomerDto;
import pl.mateuszstefanski.dto.mapper.customer.CustomerMapper;
import pl.mateuszstefanski.service.DomainServiceInterface;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CustomersService implements DomainServiceInterface<CustomerDto> {


    private final CustomerMapper customerMapper;
    private final CustomersRepoProxyService customersRepoProxyService;

    @Override
    public CustomerDto addNew(final CustomerDto customerDto) {
        var newCustomer = customerMapper.mapFromDtoToDomain(customerDto);
        newCustomer.setHasHistory(false);
        customersRepoProxyService.checkIfCustomerNotExistsInDatabase(newCustomer);
        customersRepoProxyService.saveCustomer(newCustomer);
        return customerDto;
    }

    @Override
    public CustomerDto getById(final Long id) {
        var customer = customersRepoProxyService.getCustomerById(id);
        return customerMapper.mapFromDomainToDto(customer);
    }

    @Override
    public CustomerDto getByNum(final String num) {
        var customer = customersRepoProxyService.getCustomerByTaxNum(num);
        return customerMapper.mapFromDomainToDto(customer);
    }

    @Override
    public List<CustomerDto> getAll() {
        return customersRepoProxyService.getAllCustomers();
    }

    @Override
    public CustomerDto updateById(final Long id, final CustomerDto customerDto) {

        var beforeUpdate = customersRepoProxyService.getCustomerById(id);
        var afterUpdate = customersRepoProxyService.updateData(beforeUpdate, customerDto);

        return customerMapper.mapFromDomainToDto(customersRepoProxyService.saveCustomer(afterUpdate));
    }

    @Override
    public CustomerDto updateByNum(final String num, final CustomerDto customerDto) {
        var beforeUpdate = customersRepoProxyService.getCustomerByTaxNum(num);
        var afterUpdate = customersRepoProxyService.updateData(beforeUpdate, customerDto);

        return customerMapper.mapFromDomainToDto(customersRepoProxyService.saveCustomer(afterUpdate));
    }

    @Override
    public void deleteById(final Long id) {
        customersRepoProxyService.deleteCustomer(id);
    }

    @Override
    public void deleteByNum(final String num) {
        customersRepoProxyService.deleteCustomerByTaxNum(num);
    }

    public void preventRemovalOfCustomer(String taxIdNum) {
        customersRepoProxyService.preventRemovalOfCustomer(taxIdNum);
    }


}
