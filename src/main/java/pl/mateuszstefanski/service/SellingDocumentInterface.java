package pl.mateuszstefanski.service;

public interface SellingDocumentInterface<T> {

    T createDocument();
    T addProductToDocument(String catalogNum, Double quantity);
    T acceptDocument();


}
