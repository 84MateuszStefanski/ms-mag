package pl.mateuszstefanski.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PercentValidator implements ConstraintValidator<Percent, String> {

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return value != null && value.matches("^-?[0-9]{1,4}.[0-9]{1,2}$");
    }

    @Override
    public void initialize(final Percent constraintAnnotation){
    }
}
