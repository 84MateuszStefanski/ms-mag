package pl.mateuszstefanski.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Constraint(validatedBy = PercentValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Percent {
    String message() default "Invalid percent";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
