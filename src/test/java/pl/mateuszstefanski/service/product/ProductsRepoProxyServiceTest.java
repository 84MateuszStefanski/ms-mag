package pl.mateuszstefanski.service.product;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import pl.mateuszstefanski.domain.product.Product;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.domain.product.SelectedProduct;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.domain.product.TypeOfArticle;
import pl.mateuszstefanski.dto.mapper.product.ProductMapper;
import pl.mateuszstefanski.exceptions.CannotBeDeletedException;
import pl.mateuszstefanski.exceptions.ProductNotFoundException;
import pl.mateuszstefanski.repository.product.ProductsRepository;

@ContextConfiguration(classes = {ProductsRepoProxyService.class})
@ExtendWith(SpringExtension.class)
class ProductsRepoProxyServiceTest {
    @MockBean
    private ProductMapper productMapper;

    @Autowired
    private ProductsRepoProxyService productsRepoProxyService;

    @MockBean
    private ProductsRepository productsRepository;

    /**
     * Method under test: {@link ProductsRepoProxyService#getIdList(List)}
     */
    @Test
    void testGetIdList() {
        assertTrue(this.productsRepoProxyService.getIdList(new ArrayList<>()).isEmpty());
    }

    /**
     * Method under test: {@link ProductsRepoProxyService#getIdList(List)}
     */
    @Test
    void testGetIdList2() {
        SelectedProduct selectedProduct = mock(SelectedProduct.class);
        when(selectedProduct.getProductId()).thenReturn(123L);
        doNothing().when(selectedProduct).setProductId((Long) any());
        doNothing().when(selectedProduct).setQuantity((Double) any());
        doNothing().when(selectedProduct).setSumNet((BigDecimal) any());
        selectedProduct.setProductId(123L);
        selectedProduct.setQuantity(1D);
        selectedProduct.setSumNet(BigDecimal.valueOf(42L));

        ArrayList<SelectedProduct> selectedProductList = new ArrayList<>();
        selectedProductList.add(selectedProduct);
        List<Long> actualIdList = this.productsRepoProxyService.getIdList(selectedProductList);
        assertEquals(1, actualIdList.size());
        assertEquals(123L, actualIdList.get(0));
        verify(selectedProduct).getProductId();
        verify(selectedProduct).setProductId((Long) any());
        verify(selectedProduct).setQuantity((Double) any());
        verify(selectedProduct).setSumNet((BigDecimal) any());
    }



    /**
     * Method under test: {@link ProductsRepoProxyService#getProductByCatalogNum(String)}
     */
    @Test
    void testGetProductByCatalogNum() {
        // Arrange
        ProductCategory productCategory = new ProductCategory();
        productCategory.setName("Name");

        Product product = new Product();
        product.setCatalogNumber("42");
        product.setCategory(productCategory);
        product.setEanCode("Ean Code");
        product.setHasHistory(true);
        product.setName("Name");
        product.setPurchaseNetPrice(BigDecimal.valueOf(42L));
        product.setPurchaseTaxRate(Tax.TAX23);
        product.setQuantity(10.0d);
        product.setSaleMarginNet(BigDecimal.valueOf(42L));
        product.setSalePriceGross(BigDecimal.valueOf(42L));
        product.setSalePriceNet(BigDecimal.valueOf(42L));
        product.setSalesTaxRate(Tax.TAX23);
        product.setStockUnit("Stock Unit");
        product.setTypeOfArticle(TypeOfArticle.GOODS);
        Optional<Product> ofResult = Optional.of(product);
        when(this.productsRepository.findProductByCatalogNumber((String) any())).thenReturn(ofResult);

        // Act
        Product actualProductByCatalogNum = this.productsRepoProxyService.getProductByCatalogNum("Catalog Num");

        // Assert
        assertSame(product, actualProductByCatalogNum);
        assertEquals("42", actualProductByCatalogNum.getPurchaseNetPrice().toString());
        assertEquals("42", actualProductByCatalogNum.getSalePriceGross().toString());
        assertEquals("42", actualProductByCatalogNum.getSalePriceNet().toString());
        assertEquals("42", actualProductByCatalogNum.getSaleMarginNet().toString());
        verify(this.productsRepository).findProductByCatalogNumber((String) any());
    }

    /**
     * Method under test: {@link ProductsRepoProxyService#getProductByCatalogNum(String)}
     */
    @Test
    void testGetProductByCatalogNum2() {
        // Arrange
        when(this.productsRepository.findProductByCatalogNumber((String) any())).thenReturn(Optional.empty());

        // Act and Assert
        assertThrows(ProductNotFoundException.class,
                () -> this.productsRepoProxyService.getProductByCatalogNum("Catalog Num"));
        verify(this.productsRepository).findProductByCatalogNumber((String) any());
    }

    /**
     * Method under test: {@link ProductsRepoProxyService#getProductByCatalogNum(String)}
     */
    @Test
    void testGetProductByCatalogNum3() {
        // Arrange
        when(this.productsRepository.findProductByCatalogNumber((String) any())).thenThrow(new CannotBeDeletedException());

        // Act and Assert
        assertThrows(CannotBeDeletedException.class,
                () -> this.productsRepoProxyService.getProductByCatalogNum("Catalog Num"));
        verify(this.productsRepository).findProductByCatalogNumber((String) any());
    }


}

