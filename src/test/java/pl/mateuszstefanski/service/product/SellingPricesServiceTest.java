package pl.mateuszstefanski.service.product;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.domain.product.TypeOfArticle;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.repository.product.ProductsRepository;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class SellingPricesServiceTest {

    @Autowired
    ProductsService productsService;
    @Autowired
    SellingPricesService pricesService;
    @Autowired
    ProductsRepository repository;

    static ProductDto product;

    @BeforeAll
    static void setup(@Autowired ProductsService productsService) {
        product = createProducts();
        productsService.createNewProduct(product);
    }


    @Test
    void should_set_price_by_margin_for_productId(){
        //given
        Long id = 3L;
        var margin = 20D;
        //when
        var result = pricesService.setPriceByMarginForProductId(id,margin);
        var product = repository.findProductByProductId(id).get();
        //then
        assertThat(product.getSalePriceNet().doubleValue()).isEqualTo(BigDecimal.valueOf(79.2).doubleValue());
        assertThat(product.getSalePriceGross().doubleValue()).isEqualTo(BigDecimal.valueOf(97.42).doubleValue());
    }

    @Test
    void should_not_change_price_by_margin_for_productId(){
        //given
        Long id = 3L;
        var margin = 0D;
        //when
        var result = pricesService.setPriceByMarginForProductId(id,margin);
        var product = repository.findProductByProductId(id).get();
        //then
        assertThat(product.getSalePriceNet().doubleValue()).isEqualTo(BigDecimal.valueOf(66.0).doubleValue());
        assertThat(product.getSalePriceGross().doubleValue()).isEqualTo(BigDecimal.valueOf(81.18).doubleValue());
    }

    @Test
    void should_set_price_by_margin_for_catalogNum(){
        //given
        var catalogNum = "YT-14471";
        var margin = 20D;
        //when
        var result = pricesService.setPriceByMarginForCatalogNum(catalogNum,margin);
        var product = repository.findProductByProductId(3L).get();
        //then
        assertThat(product.getSalePriceNet().doubleValue()).isEqualTo(BigDecimal.valueOf(79.2).doubleValue());
        assertThat(product.getSalePriceGross().doubleValue()).isEqualTo(BigDecimal.valueOf(97.42).doubleValue());

    }

    @Test
    void should_set_margin_by_sell_price_nett_for_productId(){
        //given
        Long id = 3L;
        var price = 79.2D;
        //when
        var result = pricesService.setMarginByPriceNetForProductId(id,price);
        var product = productsService.getProductByProductId(id);
        //then
        assertThat(product.getSaleMarginNet().doubleValue()).isEqualTo(20D);
    }

    @Test
    void should_set_margin_by_sell_price_nett_for_catalogNum(){
        //given
        var catalogNum = "YT-14471";
        var price = 79.2D;
        //when
        var result = pricesService.setMarginByPriceNetForCatalogNum(catalogNum,price);
        var product = productsService.getProductByProductId(3L);
        //then
        assertThat(product.getSaleMarginNet().doubleValue()).isEqualTo(20D);
    }

//    @Test
//    void should_count_zero_margin_by_sell_price_nett_for_catalogNum(){
//        //given
//        var catalogNum = "YT-14471";
//        var price = 66.0D;
//        //when
//        var product = productsService.getProductByProductId(3L);
//        //then
//        assertThat(product.getSaleMarginNet().doubleValue()).isEqualTo(0);
//    }

    @Test
    void should_count_margin_by_price_gross_for_product_id(){
        //given
        Long id = 3L;
        var price = 97.42D;
        //when
        var result = pricesService.setMarginByPriceGrossForProductId(id, price);
        var product = productsService.getProductByProductId(id);
        //then
        assertThat(product.getSaleMarginNet().doubleValue()).isEqualTo(20D);

    }

    private static ProductDto createProducts() {
        return ProductDto.builder()
                .name("Tool set")
                .catalogNumber("YT-14471")
                .category(ProductCategory.builder()
                        .name("Tool-sets")
                        .build())
                .eanCode("444587415")
                .purchaseNetPrice(BigDecimal.valueOf(66.00D))
                .purchaseTaxRate(Tax.TAX23)
                .salesTaxRate(Tax.TAX23)
                .stockUnit("set")
                .quantity(0)
                .typeOfArticle(TypeOfArticle.GOODS)
                .salePriceNet(BigDecimal.ZERO)
                .salePriceGross(BigDecimal.ZERO)
                .saleMarginNet(BigDecimal.ZERO)
                .build();
    }
}
