package pl.mateuszstefanski.service.product;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mateuszstefanski.domain.product.ProductCategory;
import pl.mateuszstefanski.domain.product.Tax;
import pl.mateuszstefanski.domain.product.TypeOfArticle;
import pl.mateuszstefanski.dto.product.ProductDto;
import pl.mateuszstefanski.repository.product.ProductsRepository;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class ProductsServiceTest {

    @Autowired
    ProductsService productsService;
    @Autowired
    ProductsRepository productsRepository;

    static ProductDto product;

    @BeforeAll
    static void setup(@Autowired ProductsService productsService) {
         product = createProducts();
         productsService.createNewProduct(product);
    }


    @Test
    void should_create_new_product(){
        //given when


        //then
        var productFromDatabaseByCatalogNum = productsRepository.findProductByCatalogNumber("YT-12681").get();
        var productFromDatabaseById = productsRepository.findById(3L).get();
        assertNotNull(productFromDatabaseByCatalogNum);
        assertEquals("YT-12681",productFromDatabaseById.getCatalogNumber());

    }

    @Test
    void should_get_all_products(){
        //given
       var products =  productsService.getAllProducts();

        //when

        //then
        assertEquals(3, products.size());
        System.out.println("===========================================================================================================================================");
        products.stream().forEach(System.out::println);
    }

    private static ProductDto createProducts() {
        return ProductDto.builder()
                .name("Tool set")
                .catalogNumber("YT-12681")
                .category(ProductCategory.builder()
                        .name("Tool-sets")
                        .build())
                .eanCode("126817415")
                .purchaseNetPrice(BigDecimal.valueOf(66.00D))
                .purchaseTaxRate(Tax.TAX23)
                .salesTaxRate(Tax.TAX23)
                .stockUnit("set")
                .quantity(0)
                .typeOfArticle(TypeOfArticle.GOODS)
                .salePriceNet(BigDecimal.ZERO)
                .salePriceGross(BigDecimal.ZERO)
                .saleMarginNet(BigDecimal.ZERO)
                .build();
    }
}
