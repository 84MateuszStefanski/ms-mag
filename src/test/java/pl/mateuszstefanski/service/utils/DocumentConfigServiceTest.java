package pl.mateuszstefanski.service.utils;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mateuszstefanski.repository.document.PlaceOfIssueRepository;
import pl.mateuszstefanski.service.document.config.DocumentConfigService;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DocumentConfigServiceTest {

    @Autowired
    DocumentConfigService configService;
    @Autowired
    PlaceOfIssueRepository repository;

    @Test
    void should_set_new_place_of_issue(){
        //given
//        var newPlace1 = PlaceOfIssueDto.builder().placeOfIssue("London").build();
//        var newPlace2 = PlaceOfIssueDto.builder().placeOfIssue("New York").build();
//        var newPlace3 = PlaceOfIssueDto.builder().placeOfIssue("Shanghai").build();
        //when
        configService.setPlaceOfDocumentIssue("London");
        configService.setPlaceOfDocumentIssue("New York");
        configService.setPlaceOfDocumentIssue("Shanghai");
        var list = repository.findAll();
        //then
        var placeFromRepo = configService.getPlaceOfDocumentIssue();
        assertThat(placeFromRepo.getPlaceOfIssue()).isEqualTo("Shanghai");
        assertThat(list.size()).isEqualTo(1);

    }

}
