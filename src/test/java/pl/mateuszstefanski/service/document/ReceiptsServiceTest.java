package pl.mateuszstefanski.service.document;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.mateuszstefanski.repository.document.ReceiptsRepository;
import pl.mateuszstefanski.repository.product.ProductsRepository;

import java.time.format.DateTimeFormatter;

@SpringBootTest
public class ReceiptsServiceTest {

    @Autowired
    ReceiptsService receiptsService;
    @Autowired
    ReceiptsRepository receiptsRepository;

    @Autowired
    ProductsRepository productsRepository;




    @Test
    void should_create_new_recipe(){
        //given

        var receipt = receiptsService.createDocument();
        receiptsService.addProductToDocument("YT-38841",2D);
        var result = receiptsService.acceptDocument();
        System.out.println(result.getReceiptNumber());
        System.out.println(result.getPlaceOfIssue());
        System.out.println(result.getDateOfIssue().format(DateTimeFormatter.ISO_LOCAL_DATE));
        result.getProducts().stream().forEach(System.out::println);
        System.out.println(result.getTotalSumNet());
        System.out.println(result.getTotalSumGross());
        System.out.println(result.getTotalTax());

        System.out.println("------------------------------------------------------------------");


        var list = receiptsRepository.findAll();

        System.out.println(list.stream().findFirst().get().getReceiptNumber());

        Assertions.assertThat(productsRepository.findProductByCatalogNumber("YT-38841").get().getQuantity()).isEqualTo(8);



    }
}
