# Changelog

All notable changes to this project will be documented in this file.

# MS-Mag 1.0.0

The first version of the program provides the following functionalities:
- user registration
- adding user data
- basic CRUD functions for contractors and products
- the function of purchasing goods by entering the purchase invoice
- sales function on a fiscal receipt
- invoice sales function for a selected contractor
- both the sales invoice and the receipt can be presented in PDF format
- contractors are selected using the tax identification number and, for products, using the catalog number.
- methods using the id number have also been prepared for the possible needs of the frontend
