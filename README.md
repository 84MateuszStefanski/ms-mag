# MS-Mag by Mateusz Stefański

A sales and warehouse management program that enables the user to purchase, sell and keep inventory records.

If You want to clone this code use the link below.
```
https://gitlab.com/84MateuszStefanski/ms-mag.git
```

## Name
MS-Mag is a warehouse and sales management program modeled on the WF-Mag program by Wapro by Asseco

## Description

The MS-Mag application was written in Java 11 using the Spring 5 framework and the Spring Boot tool and the Lombok library.
Maven was used as a tool for building the application and dependencies.
The Postgresql relational database was used.
Support in Api documentation is provided by Swagger.
The tests were written using the JUnit5 library and the DiffBlue tool
MapStruct and manual mappers were used to map the objects.
PDF file generation is done thanks to com.github.librepdf openpdf.

## Usage
To use MS-Mag you must first register as a new user.
After that, complete the user's data using the appropriate endpoint.
Now you can start using the program freely.
You can learn more about the current functionalities in the changelog.md file located in the main directory.
All endpoints can be viewed and tested via the Swagger located in the main directory in the swagger.yml file.

## Authors and acknowledgment
Created by Mateusz Stefański.
Many thanks to all those who helped or in some way contributed to this app.
Especially google.com and stackoverflow.com :)

## License
My own license.

## Project status
The application is in the first stage of development and has been equipped with basic functions that will be developed and added over time.
